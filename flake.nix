{
  description = "Yann's NixOS configs";
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs?ref=nixos-24.11";
    nixpkgs-acme-tls-pr-patch = {
      url =
        # Yann's PR before squashing and rebasing onto master 28.02.2025 (rebased doesn't apply properly to NixOS 24.11)
        "https://github.com/nobodyinperson/nixpkgs/commit/a310b2681d3f33c942de5bf495b73ebdb344225d.patch";
      flake = false;
    };
    nixpkgs-master.url = "github:nixos/nixpkgs?ref=master";
    nixpkgs-staging.url = "github:nixos/nixpkgs?ref=staging";
    nixpkgs-unstable.url = "github:nixos/nixpkgs?ref=nixos-unstable";
    nixpkgs-unstable-small.url =
      "github:nixos/nixpkgs?ref=nixos-unstable-small";
    # Yann's nixpkgs fork (with PR https://github.com/NixOS/nixpkgs/pull/340136)
    # nixpkgs-yann.url = "github:nobodyinperson/nixpkgs?ref=yann";
    # nixpkgs-yann.url = "path:/home/yann/code/nixpkgs/yann";
    home-manager.url = "github:nix-community/home-manager?ref=release-24.11";
    home-manager.inputs.nixpkgs.follows = "nixpkgs";
    poetry2nix.url = "github:nix-community/poetry2nix";
    disko.url = "github:nix-community/disko";
    gitlab-backup = { # private repo, can't use fetchFromGitlab 😑
      # TODO: sometimes the leading ./ needs to be removed or re-added for the flake to eat it 🤷
      url = "git+file:pkgs/python/gitlab-backup";
      flake = false;
    };
  };
  outputs = { self, ... }@inputs:
    let
      system = "x86_64-linux";
      ### 👇 🤸 A stunt to patch nixpkgs with my PR ###
      # 'instantiate' (or what!?) nixpkgs to make it work
      nixpkgs' = import inputs.nixpkgs { inherit system; };
      # nixpkgsPatched' is a new instance of nixpkgs, but with the patch
      nixpkgsPatched' = nixpkgs'.applyPatches {
        name = "nixpkgs-with-acme-tls-mode";
        src = inputs.nixpkgs;
        patches = [ inputs.nixpkgs-acme-tls-pr-patch ];
      };
      # this makes lib.nixosSystem available again 🤷
      nixpkgsPatched = (import "${nixpkgsPatched'}/flake.nix").outputs {
        inherit (inputs) self;
      };
      ### 👆 🤸 end stunt ###
      # 🔧 A helper to reduce boilerplate with NixOS systems below
      mkSystem =
        { pkgs ? inputs.nixpkgs, modules ? [ ], system ? "x86_64-linux" }:
        pkgs.lib.nixosSystem {
          inherit system;
          specialArgs = { inherit self inputs system; };
          modules =
            # default modules included in all systems
            [
              inputs.disko.nixosModules.default # 💽 disko
              inputs.home-manager.nixosModules.default # 🏡 Home Manager NixOS module
              {
                # 🏡 Home Manager modules
                home-manager.sharedModules = [{
                  # make all of our own home manager modules available
                  imports = builtins.attrValues self.outputs.homeManagerModules;
                  home.stateVersion = "24.11";
                }];
              }
            ] ++ modules;
        };
    in {
      # 🖥️ My NixOS machines
      nixosConfigurations = {
        yann-desktop-5u18 = mkSystem {
          pkgs = nixpkgsPatched;
          modules = [ hosts/uni-tuebingen/yann-desktop-5u18/configuration.nix ];
        };
        yann-ideapad-16 = mkSystem {
          modules = [ hosts/laptops/yann-ideapad/configuration.nix ];
        };
        yann-ideapad-14 = mkSystem {
          modules = [ hosts/uni-tuebingen/yann-ideapad-14/configuration.nix ];
        };
        homelab =
          mkSystem { modules = [ hosts/servers/homelab/configuration.nix ]; };
        installer = mkSystem { modules = [ hosts/installer/installer.nix ]; };
      };
      # 🏡 Home Manager modules
      homeManagerModules = {
        thunar-custom-actions =
          import modules/home-manager/thunar-custom-actions.nix;
      };
    };
}
