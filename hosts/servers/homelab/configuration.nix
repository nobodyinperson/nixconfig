{ pkgs, config, lib, ... }: {
  imports = [
    ./disko
    ./hardware-configuration.nix
    ../../../pkgs/overlays.nix
    ../../../modules
    ./services
  ];
  yann.collections = {
    coding.enable = true;
    productivity.enable = true;
    # science.enable = true; # datalad tests slow 😩
  };
  environment.systemPackages = with pkgs; [
    amdgpu_top
    rocmPackages.rocm-smi
    scanshop
  ];
  # Bootloader
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;
  networking.hostName = "homelab";
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "24.11"; # Did you read the comment?
}
