{
  disko.devices = {
    disk = {
      "4TB-HDD" = {
        type = "disk";
        device = "/dev/disk/by-id/usb-WDC_WD40_EFRX-68WT0N0_0000000000D6-0:0";
        content = {
          type = "gpt";
          partitions = {
            data = {
              size = "100%";
              content = {
                type = "btrfs";
                extraArgs = [ "-f" ]; # Override existing partition
                # Subvolumes must set a mountpoint in order to be mounted,
                # unless their parent is mounted
                subvolumes = {
                  "/root" = {
                    mountOptions = [ "compress=zstd" ];
                    mountpoint = "/mnt/4TB-WD";
                  };
                };
              };
            };
          };
        };
      };
    };
  };
}
