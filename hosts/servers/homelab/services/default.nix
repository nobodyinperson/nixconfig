{ pkgs, config, lib, inputs, system, ... }:
let borgMediaRepo = "/var/lib/borgbackup/media";
in {
  imports = [ ./secrets.nix ];
  config = {
    # 🔧 Maintenance
    services.btrfs.autoScrub.enable = true;
    # 📧 Mail
    age.secrets."mail-one".file = ../../../../modules/secrets/passwords/one.age;
    programs.msmtp = {
      enable = true;
      accounts.default = config.yann.secrets.msmtp.one;
    };
    # 🌐 Web
    security.acme.defaults = { email = "nobodyinperson@posteo.de"; };
    yann.services.web.domain = "nobodyinperson.mooo.com";
    age.secrets."freedns-token-nobodyinperson.mooo.com".file =
      ../../../../modules/secrets/credentials/freedns-token-nobodyinperson.mooo.com.age;
    yann.services.web.freedns = {
      enable = true;
      interval = "*:0/15";
      tokenFile =
        config.age.secrets."freedns-token-nobodyinperson.mooo.com".path;
    };
    yann.services.web.server = {
      enable = true;
      public = true;
    };
    # 🔨 Forgejo
    yann.services.web.forgejo = {
      enable = true;
      public = true;
      stateDir = "/var/lib/forgejo";
      logo = ./forgejo-logo.svg;
      backup = {
        enable = true;
        to = borgMediaRepo;
      };
    };
    services.forgejo = {
      settings = { DEFAULT.APP_NAME = lib.mkForce "Yann's Forgejo"; };
    };
    # 📝🚫 Paperless
    age.secrets."homelab-paperless-admin-password".file =
      ../../../../modules/secrets/passwords/homelab-paperless-admin.age;
    yann.services.web.paperless = {
      enable = true;
      public = true;
      publicPort = 2425;
      moduleConfig = {
        dataDir = "/var/lib/paperless";
        passwordFile =
          config.age.secrets."homelab-paperless-admin-password".path;
        consumptionDirIsPublic = true;
        settings = {
          PAPERLESS_TASK_WORKERS = 6; # parallel processing
          PAPERLESS_CONSUMER_IGNORE_PATTERN = [ ".DS_STORE/*" "desktop.ini" ];
          PAPERLESS_OCR_LANGUAGE = "deu+eng";
          PAPERLESS_OCR_USER_ARGS = {
            optimize = 1;
            pdfa_image_compression = "lossless";
          };
        };
      };
      backup = {
        enable = true;
        to = borgMediaRepo;
      };
    };
    # 🦙 Ollama
    services.ollama = {
      enable = true;
      # # Ollama with PR https://github.com/ollama/ollama/pull/6282 applied for better support of this machine
      # # Takes 30min to compile stuff and is even slower!? No GTT usage visible in amdgpu_top!? 🤷 Doesn't seem worth it...
      # package =
      #   # ran into https://github.com/NixOS/nixpkgs/issues/368672, unstable-small apparently has the fix already
      #   inputs.nixpkgs-unstable-small.legacyPackages."${system}".ollama.overrideAttrs
      #   (oldAttrs: {
      #     patches = oldAttrs.patches ++ [
      #       # TODO: This patch might fail to apply at some point!
      #       (pkgs.fetchurl {
      #         # 08.11.2024, "rewrite for ollama 4.0" in Maciej-Mogilany:AMD_APU_GTT_memory
      #         url =
      #           "https://github.com/ollama/ollama/commit/bdd7e2f91adcf485a505b929f0d6a5d66a93e2e1.patch";
      #         hash = "sha256-7UIsOGoBTnJVFgSnXZdxY+VV43liI9iwwoDt0ADRC1A=";
      #       })
      #     ];
      #   });
      user = "ollama";
      host = "0.0.0.0"; # allow external connections
      openFirewall = true;
      environmentVariables = {
        OLLAMA_MAX_LOADED_MODELS = "1";
        OLLAMA_NUM_PARALLEL = "1";
      };
      # we want GPU acceleration
      # Looking at `watch -n0 rocm-smi` or `amdgpu-top` it does actually use the GPU, but it's roughly as fast as with CPU.
      # GPU has much lower 'load duration', slightly slower 'eval rate', but overall takes a bit less time.
      # Weirdly, llava:7b (not 13b or 34b!) sometimes dies on images? 😵
      # I guess I need to tweak BIOS settings and give it more "UMA" / shared VRAM or whatever?
      acceleration = "rocm";
      # taken from https://github.com/alexhegit/Playing-with-ROCm/blob/main/inference/LLM/Run_Ollama_with_AMD_iGPU780M-QuickStart.md
      rocmOverrideGfx =
        "11.0.2"; # 11.0.3 seen in amdgpu_top- but that doesn't work
    };
    # 🛒 ScanShop
    age.secrets."scanshop-environment".file =
      ../../../../modules/secrets/credentials/scanshop-environment.age;
    yann.services.scanshop = {
      enable = true;
      device = "nt.usb.keyboard";
      extraFlags = [ "-vvv" ]; # for debugging
      environmentFile = config.age.secrets."scanshop-environment".path;
    };
    # 🔋 Uninterruptible Power Supply
    age.secrets."eaton-ups-password".file =
      ../../../../modules/secrets/passwords/eaton-ups-password.age;
    power.ups = {
      enable = true;
      mode = "standalone";
      ups."eaton-3s-550" = {
        driver = "usbhid-ups";
        description = "Eaton 3S 550";
        port = "auto";
        directives = [ "vendorid = 0463" "productid = ffff" ];
      };
      # This eaton user is apparently internal to ups{d,mon} and they both need a shared password I guess 🤷
      users."eaton".passwordFile = config.age.secrets."eaton-ups-password".path;
      upsmon.monitor."eaton-3s-550" = { user = "eaton"; };
    };
    yann.services.cpufreq-schedule = {
      enable = true;
      times."*-*-* 18:00:00" = "2.5GHz"; # night: throttle down to be quiet
      times."*-*-* 08:00:00" = "6GHz"; # day: max power allowed
    };
    # periodically TRIM mounted SSDs
    services.fstrim.enable = true;
  };
}
