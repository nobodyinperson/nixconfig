{ config, pkgs, lib, ... }: {
  imports = [ # Include the results of the hardware scan.
    ./disko.nix
    ./hardware-configuration.nix
    ../../../modules
    ../../../pkgs/overlays.nix
  ];
  services.cpupower-gui.enable = true;
  networking.networkmanager.wifi.powersave = false;
  yann.desktop = {
    enable = true; # This is a desktop system
    programs.gnome-boxes.enable = true;
  };
  yann.collections = {
    laptop.enable = true; # This is a laptop
    cad.enable = true;
    productivity.enable = true;
    microcontrollers.enable = true;
    coding.enable = true;
    science.enable = true;
    multimedia.enable = true;
  };
  # 🦙 Ollama (adjusted from homelab, same CPU/GPU)
  services.ollama = {
    enable = true;
    # # Ollama with PR https://github.com/ollama/ollama/pull/6282 applied for better support of this machine
    # # Takes 30min to compile stuff and is even slower!? No GTT usage visible in amdgpu_top!? 🤷 Doesn't seem worth it...
    # package =
    #   # ran into https://github.com/NixOS/nixpkgs/issues/368672, unstable-small apparently has the fix already
    #   inputs.nixpkgs-unstable-small.legacyPackages."${system}".ollama.overrideAttrs
    #   (oldAttrs: {
    #     patches = oldAttrs.patches ++ [
    #       # TODO: This patch might fail to apply at some point!
    #       (pkgs.fetchurl {
    #         # 08.11.2024, "rewrite for ollama 4.0" in Maciej-Mogilany:AMD_APU_GTT_memory
    #         url =
    #           "https://github.com/ollama/ollama/commit/bdd7e2f91adcf485a505b929f0d6a5d66a93e2e1.patch";
    #         hash = "sha256-7UIsOGoBTnJVFgSnXZdxY+VV43liI9iwwoDt0ADRC1A=";
    #       })
    #     ];
    #   });
    user = "ollama";
    environmentVariables = {
      OLLAMA_MAX_LOADED_MODELS = "1";
      OLLAMA_NUM_PARALLEL = "1";
    };
    # we want GPU acceleration
    # Looking at `watch -n0 rocm-smi` or `amdgpu-top` it does actually use the GPU, but it's roughly as fast as with CPU.
    # GPU has much lower 'load duration', slightly slower 'eval rate', but overall takes a bit less time.
    # Weirdly, llava:7b (not 13b or 34b!) sometimes dies on images? 😵
    # I guess I need to tweak BIOS settings and give it more "UMA" / shared VRAM or whatever?
    acceleration = "rocm";
    # taken from https://github.com/alexhegit/Playing-with-ROCm/blob/main/inference/LLM/Run_Ollama_with_AMD_iGPU780M-QuickStart.md
    rocmOverrideGfx =
      "11.0.2"; # 11.0.3 seen in amdgpu_top- but that doesn't work
  };
  # Bootloader
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;
  networking.hostName = "yann-ideapad-14"; # Define your hostname.
  # List packages installed in system profile. To search, run:
  environment.systemPackages = with pkgs; [ powertop amdgpu_top ];
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "24.11"; # Did you read the comment?
}
