{ pkgs, config, ... }: {
  imports = [
    ./hardware.nix
    ../../../modules
    ../../../pkgs/overlays.nix
    ./services.nix
    ./vital.nix
    ./umphy.nix
  ];
  # Software
  yann.desktop = {
    enable = true; # This is a desktop system
    programs.gnome-boxes.enable = true;
  };
  yann.ecryptfs.enable = true; # tools to encrypt home
  yann.collections = {
    cad.enable = true;
    productivity.enable = true;
    microcontrollers.enable = true;
    coding.enable = true;
    science.enable = true;
    multimedia.enable = true;
  };
  # networking
  networking.hostName = "yann-desktop-nixos"; # Define your hostname.
  networking.domain = "sna94.uni-tuebingen.de"; # Domain
  # networking.firewall.allowedUDPPorts = [ 53 67 ]; # DNS and DHCP for hotspot
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "23.05"; # Did you read the comment?
}
