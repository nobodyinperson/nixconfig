{ pkgs, config, ... }: {
  age.secrets.zdv.file = ../../../modules/secrets/credentials/zdv.age;
  # For mount.cifs, required unless domain name resolution is not needed.
  environment.systemPackages = with pkgs; [ cifs-utils mission-planner ];
  fileSystems."/mnt/zdv" = {
    device = "//storage.geo.uni-tuebingen.de/mn030116/DataAgBange";
    fsType = "cifs";
    options = let
      # this line prevents hanging on network split
      automountOpts =
        "x-systemd.automount,noauto,x-systemd.idle-timeout=60,x-systemd.device-timeout=5s,x-systemd.mount-timeout=5s";
      permissionOpts =
        "user,users,noperm,gid=${toString config.users.groups.users.gid}";
      credentialsOpts = "credentials=${config.age.secrets.zdv.path}";
    in [
      "${automountOpts},${permissionOpts},${credentialsOpts},nobrl,mfsymlinks,cache=none,nolease"
    ];
  };
}
