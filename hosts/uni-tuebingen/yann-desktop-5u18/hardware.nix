{ config, lib, pkgs, modulesPath, ... }: {
  imports = [ (modulesPath + "/installer/scan/not-detected.nix") ];
  # boot settings
  boot.initrd.availableKernelModules =
    [ "xhci_pci" "ahci" "nvme" "usb_storage" "usbhid" "sd_mod" ];
  boot.initrd.kernelModules = [ ];
  boot.kernelModules = [ "kvm-intel" ];
  boot.extraModulePackages = [ ];
  # Bootloader
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;
  # filesystems
  fileSystems = {
    # root
    "/" = {
      device = "/dev/disk/by-uuid/16abdad8-563f-4b21-99ea-21e7753a86aa";
      fsType = "ext4";
    };
    # boot
    "/boot" = {
      device = "/dev/disk/by-uuid/5998-C331";
      fsType = "vfat";
    };
    # extra HDDs
    "/mnt/WD-4TB-WCC5Z0XKZ9EF" = {
      device = "/dev/disk/by-uuid/8984ddb3-bec0-498c-815c-a0887d186231";
      fsType = "ext4";
    };
    "/mnt/SG-4TB-Z304S2ET" = {
      device = "/dev/disk/by-uuid/07075a57-7ca9-4fa6-a8c7-8483c2babe87";
      fsType = "ext4";
    };
    "/mnt/SG-1TB-Z9AQGZGR" = {
      device = "/dev/disk/by-uuid/28c1c43c-223f-45df-8b1b-8a524d3d28e5";
      fsType = "ext4";
    };
  };
  swapDevices =
    [{ device = "/dev/disk/by-uuid/41bd748b-606a-4d4f-8a5f-74ad63ebfe2f"; }];
  # USB links
  services.udev.extraRules = ''
    KERNEL=="sd*", SUBSYSTEM=="block", ENV{DEVTYPE}=="disk", ENV{ID_PATH}=="pci-0000:00:14.0-usb-0:11:1.0-scsi-0:0:0:0", SYMLINK+="sdcard"
    KERNEL=="sd*", SUBSYSTEM=="block", ENV{DEVTYPE}=="disk", ENV{ID_SERIAL}=="NORELSYS_1081CS0_0123456789ABCDE-0:0", SYMLINK+="µSD-anker"
  '';
  # Enables DHCP on each ethernet and wireless interface. In case of scripted networking
  # (the default) this is the recommended approach. When using systemd-networkd it's
  # still possible to use this option, but it's recommended to use it in conjunction
  # with explicit per-interface declarations with `networking.interfaces.<interface>.useDHCP`.
  networking.useDHCP = lib.mkDefault true;
  # networking.interfaces.enp0s31f6.useDHCP = lib.mkDefault true;
  nixpkgs.hostPlatform = lib.mkDefault "x86_64-linux";
  powerManagement.cpuFreqGovernor = lib.mkDefault "powersave";
  hardware.cpu.intel.updateMicrocode =
    lib.mkDefault config.hardware.enableRedistributableFirmware;
  # 🧹💾 periodically TRIM mounted SSDs
  services.fstrim.enable = true;
}
