{ pkgs, lib, config, ... }:
with lib; {
  yann.services.web.forgejo = {
    enable = true;
    public = true;
    stateDir = "/mnt/WD-4TB-WCC5Z0XKZ9EF/forgejo";
    logo = ./forgejo-logo-umphy.svg;
    templateDir = ./templates;
    subpath = null; # no subpath
    backup = rec {
      enable = true;
      RequiresMountsFor = "/mnt/SG-4TB-Z304S2ET";
      to = "${RequiresMountsFor}/borgbackup/forgejo";
    };
  };
  age.secrets.forgejo-mailer-password = {
    file = ../../../../modules/secrets/credentials/zdv-password.age;
    mode = "400";
    owner = config.services.forgejo.user;
  };
  services.forgejo = {
    settings = {
      DEFAULT.APP_NAME = lib.mkForce "UmphyLab";
      server.DOMAIN = lib.mkForce "umphylab.de";
      server.ROOT_URL = lib.mkForce "https://%(DOMAIN)s";
      "ui.meta".DESCRIPTION =
        "Where the Tübingen Umphy workgroup puts code and data";
      mailer = rec {
        ENABLED = true;
        SMTP_ADDR = "smtpserv.uni-tuebingen.de";
        SMTP_PORT = "587";
        FROM =
          "${config.services.forgejo.settings.DEFAULT.APP_NAME} <noreply@${config.services.forgejo.settings.server.DOMAIN}>";
        USER = "epubu01";
      };
      "repository.upload" = {
        FILE_MAX_SIZE = "5000";
      }; # allow larger files to be uploaded
      service.ENABLE_NOTIFY_MAIL = true;
    };
    secrets.mailer.PASSWD = config.age.secrets.forgejo-mailer-password.path;
  };
  # domain umphylab.de points to this forgejo
  services.nginx.virtualHosts."umphylab.de" = {
    forceSSL = true;
    enableACME = true; # LetsEncrypt Integration
    locations = let
      errorMessages = {
        "502".title = "Forgejo is not running";
        "502".text =
          "Either something is broken, a backup is running or Yann is fooling around. Try again later.";
      };
      errorSubPath = "/errors";
      mkErrorPageFileName = code: "${code}-error.html";
      errorPages = mapAttrs (code: error:
        let title = error.title or "Error ${code}";
        in pkgs.writeTextDir "${errorSubPath}/${mkErrorPageFileName code}" ''
          <html>
          <head>
          <meta><title>${title}</title></meta>
          <style>
            html, body { height: 100%; }
            html { display: table; margin: auto; }
            body { display: table-cell; vertical-align: middle; }
          </style>
          </head>
          <body>
          <center>
          <h1>${title}</h1>
          ${error.text or "💥"}
          </center>
          </body>
          </html>
        '') errorMessages;
      errorConfig = concatLines (mapAttrsToList (code: page:
        "error_page ${code} /${errorSubPath}/${
          builtins.baseNameOf (mkErrorPageFileName code)
        };") errorPages);
    in {
      "/" = {
        proxyPass =
          "http://127.0.0.1:${toString config.yann.services.web.forgejo.port}";
        extraConfig = errorConfig;
      };
      "/${errorSubPath}" = {
        root = pkgs.symlinkJoin { # puts all error pages into one directory
          name = "error-pages";
          paths = builtins.attrValues errorPages;
        };
        extraConfig = "internal;";
      };
    };
  };
  # TODO: only for umphylab? Not for the entire server? 🤔
  services.nginx.clientMaxBodySize = "${
      config.services.forgejo.settings."repository.upload".FILE_MAX_SIZE
    }M"; # match FILE_MAX_SIZE above
  services.nginx.virtualHosts."yann-desktop-nixos.sna94.uni-tuebingen.de" = {
    # ➡️  Redirect old links to umphylab.de
    locations."/forgejo".extraConfig =
      "rewrite ^/forgejo(.*)$ https://umphylab.de$1 permanent;";
  };
  yann.services.web.server.homepage.links."🔨 ${config.services.forgejo.settings.DEFAULT.APP_NAME} (Code, Data & Assets)" =
    lib.mkForce "https://umphylab.de";
}
