{ lib, config, ... }: {
  imports = [ ./forgejo ];
  age.secrets."paperless-admin-password".file =
    ../../../modules/secrets/passwords/paperless-admin.age;
  services.ollama = {
    enable = true;
    # acceleration = "cuda"; # wants to recompile a lot of stuff, CPU-only is fine for now
    user = "ollama";
    home = "/mnt/WD-4TB-WCC5Z0XKZ9EF/ollama";
  };
  security.acme.defaults = {
    email = "yann-georg.buechau@uni-tuebingen.de";
    tlsMode = true; # Uni Tübingen blocks port 80, so we need tlsMode
    conflictingServices = [ "nginx.service" ];
  };
  # open DHCP in firewall to make wifi hotspots work
  networking.firewall.allowedUDPPorts = [ 67 68 ];
  # 📧 Mail
  age.secrets."mail-two".file = ../../../modules/secrets/passwords/two.age;
  programs.msmtp = {
    enable = true;
    accounts.default = config.yann.secrets.msmtp.two // {
      from = config.yann.services.web.domain;
    };
  };
  age.secrets."netcup-ccp-env".file =
    ../../../modules/secrets/credentials/nc.age;
  yann.services = {
    mqtt.enable = true; # internal MQTT broker for testing
    web = {
      netcup-setdns = {
        enable = true;
        updates = [
          {
            "umphylab.de".host = "*";
            "umphylab.de".type = "A";
            "umphylab.de".value = "public-ipv4"; # set to current ipv4
          }
          {
            "umphylab.de".host = "@";
            "umphylab.de".type = "A";
            "umphylab.de".value = "public-ipv4"; # set to current ipv4
          }
        ];
        environmentFile = config.age.secrets."netcup-ccp-env".path;
      };
      server.public = true; # web server open to outside
      server.homepage = {
        title = "🛝 Yann's NixOS service playground";
        prolog = [ "Here I test out some services hosted with NixOS." ];
        epilog = [
          ''
            You can also find me <a href="https://fosstodon.org/@nobodyinperson" rel="me">on 🐘 Mastodon</a>''
        ];
      };
      jupyterhub = {
        # Database migration problem
        # (jupyterhub upgrade-db [--config ...] [--db ...] in any variant fails with "No module named 'jupyterhub'" 🤷)
        # disable for now
        enable = false;
        public = true;
      };
      homebox = {
        enable = false; # not needed currently
        public = true;
        dataDir = "/mnt/WD-4TB-WCC5Z0XKZ9EF/homebox";
        backup = rec {
          enable = true;
          RequiresMountsFor = "/mnt/SG-4TB-Z304S2ET";
          to = "${RequiresMountsFor}/borgbackup/homebox";
        };
        # settings.HBOX_OPTIONS_ALLOW_REGISTRATION = "true";
      };
      paperless = {
        enable = false; # not needed currently
        public = true;
        moduleConfig = {
          dataDir = "/mnt/WD-4TB-WCC5Z0XKZ9EF/paperless";
          passwordFile = config.age.secrets."paperless-admin-password".path;
          settings = {
            PAPERLESS_CONSUMER_IGNORE_PATTERN = [ ".DS_STORE/*" "desktop.ini" ];
            PAPERLESS_OCR_LANGUAGE = "deu+eng";
            PAPERLESS_OCR_USER_ARGS = {
              optimize = 1;
              pdfa_image_compression = "lossless";
            };
          };
        };
        backup = rec {
          enable = true;
          RequiresMountsFor = "/mnt/SG-4TB-Z304S2ET";
          to = "${RequiresMountsFor}/borgbackup/paperless";
        };
      };
    };
  };
}
