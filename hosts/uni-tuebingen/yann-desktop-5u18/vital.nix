{ pkgs, config, lib, ... }:
let
  backup_location = "/mnt/SG-4TB-Z304S2ET";
  uas_data_on_zdv = "/mnt/zdv/Data/vital/uas-data";
in {
  age.secrets = {
    "atris-token.env" = {
      file = ../../../modules/secrets/credentials/atris-token.env.age;
      owner = "yann";
    };
    "ssh-atris.fz-juelich.de-yann.buechau.tue" = {
      file =
        ../../../modules/secrets/ssh/ssh-atris.fz-juelich.de-yann.buechau.tue.age;
      owner = "yann";
    };
  };
  # Periodically clone/fetch/update all vital repos from ATRIS to the backup disk, including annex and integrity check
  systemd.services."atris-vital-backup" = {
    description = "ATRIS VITAL orga backup";
    # path = [ pkgs.file pkgs.gawk ]; # was needed for some reason?
    startAt = "*-*-* 00,08,16:00:00";
    script = ''
      ${
        pkgs.lib.getExe pkgs.git-backup
      } --gitea https://atris.fz-juelich.de --group vital -o ${backup_location} --bare --with-annex --with-fsck '1m 7d' --annex-jobs 2
    '';
    serviceConfig = {
      Type = "simple";
      EnvironmentFile = config.age.secrets."atris-token.env".path;
      User = "yann";
    };
    environment.COLUMNS = "120";
    environment.SSH_IDENTITY =
      config.age.secrets."ssh-atris.fz-juelich.de-yann.buechau.tue".path;
    unitConfig = { RequiresMountsFor = "${backup_location}"; };
  };
  # Periodically export a snapshot of the vital/uas-data repo from the backup location to the ZDV server
  systemd.services."export-uas-data-to-umphy-zdv" = {
    description =
      "Export current VITAL uas-data repo snapshot to Umphy ZDV server";
    startAt = "*-*-* 01,09,17:00:00";
    # (TODO: It doesn't work like this...) wait until repo is updated from the above service
    after = [ "atris-vital-backup.service" ];
    script = let
      uas-data-exporter = pkgs.writeShellApplication {
        name = "uas-data-exporter";
        runtimeInputs = with pkgs; [ git git-annex openssh ];
        text = ''
          set -x
          cd ${lib.escapeShellArg backup_location}/vital/uas-data
          git annex enableremote ZDV-dump directory=${
            lib.escapeShellArg uas_data_on_zdv
          }
          # git-annex <=10.20250116 has a bug that exporting synced/* branches
          # https://git-annex.branchable.com/bugs/Can__39__t_export_synced__47___branches/
          # Is fixed in the next version, but doesn't help here as pulling in a bare repo doesn't update those anyway...
          git annex pull || true # get latest data, but won't update local main branch
          git branch -f main umphylab/main # update our main branch
          git config remote.ZDV-dump.annex-tracking-branch main
          git annex push ZDV-dump
        '';
      };
    in "${lib.getExe uas-data-exporter}";
    serviceConfig = { User = "yann"; };
    unitConfig = { RequiresMountsFor = [ backup_location "/mnt/zdv" ]; };
  };
  # 📦
  environment.systemPackages = [ pkgs.git-backup ];
}
