{ config, pkgs, lib, ... }: {
  imports = [ # Include the results of the hardware scan.
    ./disko.nix
    ./hardware-configuration.nix
    ../../../modules
    ../../../pkgs/overlays.nix
  ];
  services.cpupower-gui.enable = true;
  networking.networkmanager.wifi.powersave = false;
  yann.desktop = {
    enable = true; # This is a desktop system
    programs.gnome-boxes.enable = true;
  };
  yann.collections = {
    laptop.enable = true; # This is a laptop
    cad.enable = true;
    productivity.enable = true;
    microcontrollers.enable = true;
    coding.enable = true;
    science.enable = true;
    multimedia.enable = true;
  };
  services.ollama = {
    enable = true;
    user = "ollama";
    # gets stuck? https://github.com/ggerganov/llama.cpp/issues/6110
    # acceleration = "rocm"; # use GPU
    # rocmOverrideGfx = "9.0.0";
    # environmentVariables = {
    #   OLLAMA_MAX_LOADED_MODELS = "1";
    #   OLLAMA_NUM_PARALLEL = "1";
    # };
  };
  users.users.silya = {
    isNormalUser = true;
    description = "Silke & Yann Büchau";
    extraGroups = [
      "networkmanager"
      "wheel"
      "silya"
      "scanner"
      "lp"
      "audio"
      "dialout"
      "input"
    ];
    # start with a certain password (to make VMs work), changed live later
    initialHashedPassword = config.yann.secrets.passwords.hashedPassword3;
    shell = pkgs.fish;
  };
  # Bootloader
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;
  networking.hostName = "yann-ideapad"; # Define your hostname.
  # List packages installed in system profile. To search, run:
  environment.systemPackages = with pkgs; [ powertop amdgpu_top ];
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "24.05"; # Did you read the comment?
}
