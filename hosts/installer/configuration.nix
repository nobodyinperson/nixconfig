{ pkgs, config, lib, ... }: {
  imports = [ ../../pkgs/overlays.nix ../../modules ];
  networking.networkmanager.enable = lib.mkForce true;
  networking.wireless.enable = lib.mkForce false;
  users.users.yann.openssh.authorizedKeys.keys = [
    "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDe6LCYyroYPacZZu+r3ZKQdPJg5kF5JFDCgQkpJ/CZjmWU5PrAuaBuGb1ZaibDuIjrey2ee2a+chc/Dvy5VfgAwTLyj34XO2+ko+xcY6w7I6TI+4wSdRyplgg0x/YziBCLhKFqOBr44O8EntuupM+18nyklFmXxGOiH8icSja+ca/P1z22Z9zDngu+9IY1nw3kb+1v66h3HcRzsDkZftf3OVscHCxUS93PKJQZTXXh2r5VQZC2PphGs3QqVmmRo7HrUrgYV0KaJ2/y8pJGuy92ohlj4pYSqG468Y3eAyRJw8mlRR18FDkUL8qJlKDNT4zgLQs6CjZk9QmMlKmJwuWoLHfc4LVpdjLf2uDBdv5U1eVVMIiQxECYNaIegNwD+EqpyTHEqS6LgdfA5D5rx+n83678S0LnXvZhmfHcIXFvGF3X8eCwzX9fn6Ss4r/aBxM7ah/zAPh80Ij3L6ZdOGqKsTzoDSjc/GSiBWb18/uAuqksypvcxcCY3StfASlMuE8= yann@yann-ideapad"
  ];
}
