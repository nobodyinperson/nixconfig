# Build an Installer with Yann's base system
# nix build -Lv .#nixosConfigurations.installer.config.system.build.isoImage
# run a VM
# nix-shell -p qemu --run 'qemu-system-x86_64 -enable-kvm -m 256 -cdrom result/iso/nixos-*.iso'
{ pkgs, modulesPath, config, inputs, lib, ... }: {
  imports = [
    (modulesPath + "/installer/cd-dvd/installation-cd-minimal.nix")
    (modulesPath + "/installer/cd-dvd/channel.nix")
    ../laptops/yann-ideapad/configuration.nix
  ];
  networking.wireless.enable = lib.mkForce false;
  isoImage.squashfsCompression = "gzip -Xcompression-level 1";
}
