{ solvespace, fetchFromGitHub }:
solvespace.overrideAttrs (oldAttrs: rec {
  version = "${oldAttrs.version}+${src.rev}"; # Set the custom version
  src = fetchFromGitHub {
    owner = "nobodyinperson"; # my fork
    repo = oldAttrs.pname;
    rev = "3e950fc9"; # Yann's wanted changes (keybindings, show only unconstrained) rebased onto solvespace master from 01.02.2025
    hash = "sha256-2EMpcGfzc7a8HxPbajhCyUI/N9sLuskojRxRlElugeg=";
    fetchSubmodules = true;
    deepClone = true;
    # from https://discourse.nixos.org/t/cant-build-solvespace-from-scratch-fetching-git-source-fails/46536/8?u=nobodyinperson
    # apparently there is a bug in fetchgit (or rather its nix-prefetch-git script buried underneath),
    # that prevents it from falling back to not using --depth on dumb http remotes
    # (which is the case for solvespace's extlib/freetype submodule on) hosted on some weird GNU savannah dumb http remote
    postFetch = ''
      echo "removing \`.git'..." >&2
      find "$out" -name .git -print0 | xargs -0 rm -rf
    '';
  };
})
