{ pkgs ? import <nixpkgs> { } }:
pkgs.hledger.overrideAttrs (oldAttrs: rec {
  version = "1.27.1"; # Latest version as of August 2023 is 1.30.1, quite farther, but bumping the version here doesn't work, the build always fails due to outdated Haskell libs being provided... 🫤
  src = fetchTarball {
    url = "https://hackage.haskell.org/package/hledger-${version}/hledger-${version}.tar.gz";
    sha256 = "sha256:027m7sgn8s7rzzcmads2qcdzs8p50lyqpa6zai9grwawx9zym7bg";
  };
})
