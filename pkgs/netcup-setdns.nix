{ writeShellApplication, curl, jq, ... }:
writeShellApplication {
  name = "netcup-setdns";
  runtimeInputs = [ curl jq ];
  text = builtins.readFile ./netcup-setdns.sh;
}
