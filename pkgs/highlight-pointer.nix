{ pkgs ? import <nixpkgs> { } }:

with pkgs;
stdenv.mkDerivation rec {
  pname = "highlight-pointer";
  version = "${src.rev}";

  src = fetchFromGitHub {
    owner = "swillner";
    repo = "highlight-pointer";
    rev = "32bf4c60696a4764e8060574ca3031f4fb4ca20d";
    sha256 = "sha256-mz9gXAtrtSV0Lapx8xBOPljuF+HRgDaF2DKCDrHXQa8=";
  };

  nativeBuildInputs = with pkgs.xorg; [ libX11 libXext libXfixes libXi ];

  buildFlags = [ ];
  installPhase = "install -Dm755 highlight-pointer -t $out/bin";

  meta = with lib; {
    description = "Highlight mouse pointer/cursor using a dot/circle";
    license = licenses.mit;
    platforms = platforms.all;
  };
}
