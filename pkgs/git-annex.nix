{ git-annex, fetchgit }:
git-annex.overrideAttrs (oldAttrs: rec {
  version = "${src.rev}";
  src = fetchgit {
    url = "git://git-annex.branchable.com/";
    rev = "10.20240531";
    sha256 = "sha256-PegjitCkdw3XqnnihD2Ki8pNuP93e3Eb8VT7nBFvSZQ=";
  };
})
