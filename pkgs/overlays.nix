{ pkgs, config, inputs, system, ... }: {
  nixpkgs.overlays = [
    (final: prev: {
      poetry2nix = inputs.poetry2nix.lib.mkPoetry2Nix { inherit pkgs; };
    })
    (final: prev: rec {
      # use certain packages from unstable
      inherit (inputs.nixpkgs-unstable.legacyPackages."${system}")
        openscad-unstable hledger git-annex freecad prusa-slicer libqalculate;
    })
    # (final: prev: {
    #   # datalat tests are too damn slow!! 😩
    #   # TODO: Is this still necessarry, now that we do the same for the Python packages below? 🤔
    #   datalad =
    #     prev.datalad.overridePythonAttrs (oldAttrs: { doCheck = false; });
    # })
    (final: prev: rec {
      git-annex-pinned = prev.callPackage (import ./git-annex.nix) {
        # Apparently 'callPackage always uses the final pkgs attrset for default function args' (see https://discourse.nixos.org/t/infinite-recursion-with-callpackage-in-overlays/9945/6)
        # so we need to explicitly give it the git-annex from prev (what the heck is all this 😅)
        inherit (prev) git-annex;
      };
      netcup-setdns = prev.callPackage ./netcup-setdns.nix { };
      yanns-nix-scripts =
        prev.callPackage (import ./nix-scripts/default.nix) { };
      latexmk-yann = prev.callPackage (import ./latexmk.nix) { };
      # custom packages
      py-gitea = (import ./python/py-gitea.nix) { pkgs = prev; };
      git-backup = (import ./python/git-backup.nix) {
        pkgs = prev;
        inherit py-gitea inputs;
      };
      scanshop = prev.callPackage (import ./python/scanshop.nix) { };
      opentimestamps-client =
        (import ./opentimestamps-client.nix) { pkgs = prev; };
      camp2ascii = (import ./camp2ascii.nix) { pkgs = prev; };
      # hledger = (import ./hledger.nix) { pkgs = prev; };
      annextimelog = prev.annextimelog.overrideAttrs (oldAttrs: rec {
        src = prev.fetchFromGitLab {
          owner = "nobodyinperson";
          repo = "annextimelog";
          rev = "f1f05cc";
          hash = "sha256-UU71EmNHHxe9PH1amk6xFNi0UGy3+B7+pWHDOAj10mM=";
        };
        version = "0.14.0+${src.rev}";
        propagatedBuildInputs = with prev.python3.pkgs; [ rich tzdata ];
      });
      solvespace = prev.callPackage (import ./solvespace.nix) {
        inherit (prev) solvespace;
      };
      stl-thumb = (import ./stl-thumb.nix) { pkgs = prev; };
      bemoji = (import ./bemoji.nix) { pkgs = prev; };
      passrofi = (import ./passrofi.nix) { pkgs = prev; };
      kbibtex = (import ./kbibtex.nix) { pkgs = prev; };
      highlight-pointer = (import ./highlight-pointer.nix) { pkgs = prev; };
    })
    # 🐍 Python packages
    (final: prev: {
      pythonPackagesExtensions = prev.pythonPackagesExtensions ++ [
        (pyfinal: pyprev: {
          jupyterlab-code-formatter =
            pyfinal.callPackage ./python/jupyterlab-code-formatter.nix;
          # Skip tests for datalad and datalad-next Python packages (just too slow 😩)
          datalad =
            pyprev.datalad.overridePythonAttrs (oldAttrs: { doCheck = false; });
          datalad-next = pyprev.datalad-next.overridePythonAttrs
            (oldAttrs: { doCheck = false; });
        })
      ];
    })
  ];
}
