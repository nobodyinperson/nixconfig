{ pkgs ? import <nixpkgs> { }, ... }:
with pkgs;
stdenv.mkDerivation rec {
  pname = "passrofi";
  version = "${src.rev}";

  src = fetchFromGitHub {
    owner = "geraldwuhoo";
    repo = "passrofi";
    rev = "3f987bb405ab43f7452a96c2aed14132a4fda6d8";
    sha256 = "sha256-AxXw1sX5ZTZpVZTE8yhlfxwMfR2rj/B5uCq7JSfClCw=";
  };

  nativeBuildInputs = [ makeWrapper ]; # needed for wrapProgram below
  buildInputs = with pkgs; [
    (pass.withExtensions (ext: with ext; [ pass-otp ]))
    rofi
  ];

  installPhase = ''
    install -Dm755 passrofi -t $out/bin
    # create wrapper with proper paths for dependencies set
    wrapProgram $out/bin/passrofi --prefix PATH : ${lib.makeBinPath buildInputs}
  '';

  meta = with lib; {
    description =
      "POSIX-compliant shell script to extend passmenu functionality to rofi with username, password, and OTP.";
    license = licenses.gpl2;
    platforms = platforms.all;
  };
}
