{ pkgs ? import <nixpkgs> { } }:
pkgs.kbibtex.overrideAttrs (oldAttrs: rec {
  version = "${oldAttrs.version}+git${src.rev}"; # Set the custom version
  src = pkgs.fetchgit {
    url = "https://invent.kde.org/thomasfischer/kbibtex.git";
    rev =
      "4ff1ad50249d58b1315f296d5866d420c8050856"; # don't resolve symlinks in linked documents
    sha256 = "sha256-XotH5kv1yVzMJQt6amq6wxSbERNUA+W3XFi4aU8j1Wg=";
  };
})
