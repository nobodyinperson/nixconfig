{ pkgs, ... }:
let
  yanns-nixpkgs = (fetchTarball {
    url =
      "https://github.com/nobodyinperson/nixpkgs/archive/8fadf683d.tar.gz"; # forgejo-add-extraRuntimeInputs
    sha256 = "sha256:0n5gfgsx3ab0l6nji545w3w2kwi4h7yd2wprbbc57nckglmhagqx";
  });
  forgejo-aneksajo-matrss-func =
    (import "${yanns-nixpkgs}/pkgs/by-name/fo/forgejo/generic.nix" {
      version = "8.0.2-git-annex0";
      rev = "3d9f6e3de5";
      hash = "sha256-NxSCL+1rDcz75XrrdTbQ/3tZk9xWkS36KY/D3bypxFw=";
      npmDepsHash = "sha256-dIf1Sh0jrQPM2IY+VtTtrEioBM1r43mQEI0fG4DcGTc=";
      vendorHash = "sha256-4l4kscwesW/cR8mZjE3G9HcVm0d1ukxbtBY6RXYRi8k=";
      lts = false;
      nixUpdateExtraArgs =
        [ "--override-filename" "pkgs/by-name/fo/forgejo/package.nix" ];
    });
  forgejo-aneksajo-with-deps = pkgs.callPackage forgejo-aneksajo-matrss-func {
    # extra dependencies forgejo should see
    extraRuntimeInputs = with pkgs; [
      git-annex # for annex support
      # the following is for external renderers
      coreutils
      gawk
      libarchive
      netcdf
      unzip
      (python3.withPackages (ps: with ps; [ jupyter nbconvert ]))
    ];
  };
in forgejo-aneksajo-with-deps.overrideAttrs (oldAttrs: { doCheck = false; })
