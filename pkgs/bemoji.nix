{ pkgs ? import <nixpkgs> { }, ... }:
with pkgs;
stdenv.mkDerivation rec {
  pname = "bemoji";
  version = "${src.rev}";

  src = fetchFromGitHub {
    owner = "marty-oehme";
    repo = "bemoji";
    rev = "4209b906699191df611a3420e410f3711c5a0540";
    sha256 = "sha256-DhsJX5HlyTh0QLlHy1OwyaYg4vxWpBSsF71D9fxqPWE=";
  };

  # These are my specific dependencies. bemoji has many optional dependencies e.g. for wayland and other launchers.
  nativeBuildInputs = [ makeWrapper ]; # needed for wrapProgram below
  buildInputs = with pkgs; [ curl rofi xclip ];

  installPhase = ''
    install -Dm755 bemoji -t $out/bin
    # create wrapper with proper paths for dependencies set
    wrapProgram $out/bin/bemoji --prefix PATH : ${lib.makeBinPath buildInputs}
  '';

  meta = with lib; {
    description =
      "Emoji picker that remembers your favorites, with support for bemenu/wofi/rofi/dmenu and wayland/X11.";
    license = licenses.mit;
    platforms = platforms.all;
  };
}
