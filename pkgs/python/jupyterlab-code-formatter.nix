{ fetchPypi, python3 }:
python3.pkgs.buildPythonPackage rec {
  pname = "jupyterlab-code-formatter";
  version = "3.0.2";
  format = "pyproject";
  src = fetchPypi {
    pname = "jupyterlab_code_formatter";
    inherit version;
    sha256 = "sha256-Va24+oub1Y8LOefT6tbB6GLp68FESmbNtCM9jcY1HUs=";
  };
  propagatedBuildInputs = with python3.pkgs; [
    hatchling
    hatch-jupyter-builder
    hatch-nodejs-version
    jupyterlab
  ];
}
