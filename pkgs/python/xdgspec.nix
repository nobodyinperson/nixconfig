{ pkgs ? import <nixpkgs> { }, ... }:
with pkgs.python3Packages;
buildPythonPackage rec {
  pname = "xdgspec";
  version = "0.2.1";
  format = "setuptools";
  src = pkgs.fetchPypi {
    inherit pname version;
    hash = "sha256-OE2Nuk4DOu+D/HKYHtuEMdsK+J3mT2zCWv5iduJ0ZBM=";
  };
  nativeBuildInputs = [ setuptools wheel ];
  propagatedBuildInputs = [];
  doCheck = false; # this is somehow necessary, otherwise that PEP517 error appears 🤷
}
