{ pkgs ? import <nixpkgs> { }, ... }:
with pkgs.python3Packages;
buildPythonPackage rec {
  pname = "py-gitea";
  version = "0.2.8";
  format = "setuptools";
  src = pkgs.fetchPypi {
    inherit pname version;
    hash = "sha256-92QeoIGFKbWfPKnDixBqXCugag9mKgIvdqYpGfF+g3k=";
  };
  nativeBuildInputs = [ setuptools wheel ];
  propagatedBuildInputs = [ requests immutabledict ];
  doCheck = false; # this is somehow necessary, otherwise that PEP517 error appears 🤷
}
