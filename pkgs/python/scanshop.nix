{ pkgs, fetchFromGitLab, poetry2nix ? import (fetchTarball
  "https://github.com/nix-community/poetry2nix/archive/refs/heads/master.zip") {
    inherit pkgs;
  }, linuxHeaders, ... }:
poetry2nix.mkPoetryApplication {
  projectDir = fetchFromGitLab {
    repo = "scanshop";
    owner = "nobodyinperson";
    rev = "3546559faaede97ef2609da036dd83d6eed4f980";
    hash = "sha256-ALS7iXsc9ah1e0inKSQkcxo4GUSVnI4fR4m/qWux6EA=";
  };
  preferWheels = true;
  overrides = poetry2nix.defaultPoetryOverrides.extend (final: prev: {
    evdev = prev.evdev.overridePythonAttrs (_old: {
      # poetry2nix's override for evdev is outdated (3 years?) and only works for evdev<1.6 or so.
      # Maybe the below also works for older evdev versions.
      # TODO: upstream this to poetry2nix
      C_INCLUDE_PATH = "${linuxHeaders}/include";
      # 👇 why does the below not work? 🤔
      # buildInputs = _old.buildInputs ++ [ linuxHeaders ];
    });
  });
  meta.mainProgram = "scanshop";
}
