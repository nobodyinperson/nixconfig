{ pkgs ? import <nixpkgs> { }, ... }:
with pkgs.python3Packages;
let
  xdgspec = import ./xdgspec.nix { inherit pkgs; };
in buildPythonPackage rec {
  pname = "polt";
  version = "1.1.0";
  format = "setuptools";
  src = pkgs.fetchPypi {
    inherit pname version;
    hash = "sha256-z5hho7sxd19ykl9/un+otyOQB6bUG01xxG38dsmg8iU=";
  };
  nativeBuildInputs = [ setuptools wheel ];
  propagatedBuildInputs =
    [ xdgspec numpy scipy matplotlib click setuptools tkinter ];
  doCheck =
    false; # this is somehow necessary, otherwise that PEP517 error appears 🤷
}
