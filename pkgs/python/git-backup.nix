{ pkgs, py-gitea, inputs, ... }:
with pkgs.python3Packages;
buildPythonPackage rec {
  pname = "git-backup";
  version = "0.0.0";
  format = "pyproject";
  src = inputs.gitlab-backup;
  nativeBuildInputs = [ poetry-core ];
  propagatedBuildInputs =
    [ pkgs.git pkgs.git-annex rich python-gitlab pkgs.openssh py-gitea ];
  meta = { mainProgram = pname; };
}
