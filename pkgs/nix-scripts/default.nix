{ stdenv }:
stdenv.mkDerivation {
  pname = "yanns-nix-scripts";
  version = "1.0";
  src = ./.;
  buildInputs = [ ];
  installPhase = ''
    mkdir -p $out/bin
    cp $src/* $out/bin
    chmod +x $out/bin/*
  '';
}
