{ pkgs ? (import <nixpkgs> { }), ... }:
with pkgs;
# Yann's fork of opentimestamps-client with proper configuration options
# and other features that doesn't get merged since 2021 for some reason
opentimestamps-client.overrideAttrs (finalAttrs: previousAttrs: {
  version = "0.7.1-yanns-pr121";
  src = fetchFromGitHub {
    owner = "nobodyinperson";
    repo = "opentimestamps-client";
    rev =
      "9d34cb1ac79668e84a55bdf0b42681dc34b77c03"; # https://github.com/opentimestamps/opentimestamps-client/pull/121
    sha256 = "sha256-vU7978jZ3hi2JUp/5YVVH5fwQJ/sMgHhMLXFB9VEApA=";
  };
})
