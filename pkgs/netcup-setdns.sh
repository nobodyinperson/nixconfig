#!/bin/bash
# adapted from https://forum.netcup.de/netcup-anwendungen/ccp-customer-control-panel/p234827-dns-%C3%A4ndern-per-api

if test -z "${APIKEY-}"; then
  echo "APIKEY not set"
  exit 1
fi
if test -z "${APIPW-}"; then
  echo "APIPW not set"
  exit 1
fi
if test -z "${CID-}"; then
  echo "CID not set"
  exit 1
fi

[ $# -lt 3 ] && echo "usage: setdns.sh domain type value [host]" && exit 1

DOMAIN=$1
TYPE=$2
VALUE=$3
HOST="@"
[ $# -gt 3 ] && HOST=$4

most_frequent() {
  sort | uniq -c | sort -nr | perl -ne 'print if s|^\s*\d+\s+||g' | head -n 1
}

quick_curl() {
  (
    set -x
    curl --max-time 5 --silent "$@"
  )
}

get_ipv4() {
  echo >&2 "Determining IPv4 address..."
  (
    (quick_curl icanhazip.com || true)
    (quick_curl httpbin.org/ip | jq -r .origin || true)
    (quick_curl https://api.ipify.org || true)
  ) | most_frequent
}

get_ipv6() {
  echo >&2 "Determining IPv6 address..."
  (
    (quick_curl https://api6.ipify.org || true)
    (quick_curl https://ipv6.getmyip.dev || true)
  ) | most_frequent
}

case "${VALUE-}" in
public-ipv4)
  VALUE="$(get_ipv4)"
  echo >&2 "IPv4 address is '$VALUE'"
  ;;
public-ipv6)
  VALUE="$(get_ipv6)"
  echo >&2 "IPv6 address is '$VALUE'"
  ;;
esac

echo >&2 "Will set '$TYPE' DNS record for domain '$DOMAIN' and host '$HOST' to '$VALUE'"

if test -z "${VALUE-}"; then
  echo >&2 "Destination needs to be non-empty"
  exit 1
fi

logout() {
  curl --no-progress-meter -X POST https://ccp.netcup.net/run/webservice/servers/endpoint.php?JSON -H 'Content-Type: application/json' -d '{
   "action":"logout",
   "param":{
      "apikey":"'"$APIKEY"'",
      "apisessionid":"'"$APISESSIONID"'",
      "customernumber":"'"$CID"'"
   }
}' | jq
}

LOGIN="$(curl --no-progress-meter -X POST https://ccp.netcup.net/run/webservice/servers/endpoint.php?JSON -H 'Content-Type: application/json' -d '{
   "action":"login",
   "param":{
      "apikey":"'"$APIKEY"'",
      "apipassword":"'"$APIPW"'",
      "customernumber":"'"$CID"'"
   }
}')"
APISESSIONID="$(echo "$LOGIN" | jq -r '.responsedata.apisessionid')"

RECORDS="$(curl --no-progress-meter -X POST https://ccp.netcup.net/run/webservice/servers/endpoint.php?JSON -H 'Content-Type: application/json' -d '{
   "action":"infoDnsRecords",
   "param":{
      "apikey":"'"$APIKEY"'",
      "apisessionid":"'"$APISESSIONID"'",
      "customernumber":"'"$CID"'",
      "domainname":"'"$DOMAIN"'"
   }
}' | jq '.responsedata.dnsrecords[] | select(.hostname=="'"$HOST"'") | select(.type=="'"$TYPE"'") | {dnsrecords: [.]}')"
echo Old Record:
echo "$RECORDS" | jq

ID="$(echo "$RECORDS" | jq '.dnsrecords[]' | jq -r '.id')"
RC="$(curl --no-progress-meter -X POST https://ccp.netcup.net/run/webservice/servers/endpoint.php?JSON -H 'Content-Type: application/json' -d '{
   "action":"updateDnsRecords",
   "param":{
      "apikey":"'"$APIKEY"'",
      "apisessionid":"'"$APISESSIONID"'",
      "customernumber":"'"$CID"'",
      "domainname":"'"$DOMAIN"'",
      "dnsrecordset":{
         "dnsrecords":[
            {
               "id":"'"$ID"'",
               "hostname":"'"$HOST"'",
               "type":"'"$TYPE"'",
               "priority":"0",
               "destination":"'"$VALUE"'",
               "deleterecord":"FALSE",
               "state":"yes"
            }
         ]
      }
   }
}')"
echo "Return: "
echo "$RC" | jq
STATUS="$(echo "$RC" | jq -r .status || true)"
MESSAGE="$(echo "$RC" | jq -r .longmessage || true)"
if test "$STATUS" != "success"; then
  echo >&2 "updating DNS unsuccessful: status = $STATUS, message = $MESSAGE"
  logout
  exit 1
fi
RECORDS="$(curl --no-progress-meter -X POST https://ccp.netcup.net/run/webservice/servers/endpoint.php?JSON -H 'Content-Type: application/json' -d '{
   "action":"infoDnsRecords",
   "param":{
      "apikey":"'"$APIKEY"'",
      "apisessionid":"'"$APISESSIONID"'",
      "customernumber":"'"$CID"'",
      "domainname":"'"$DOMAIN"'"
   }
}' | jq '.responsedata.dnsrecords[] | select(.hostname=="'"$HOST"'") | select(.type=="'"$TYPE"'") | {dnsrecords: [.]}')"
echo New Record:
echo "$RECORDS" | jq
logout
