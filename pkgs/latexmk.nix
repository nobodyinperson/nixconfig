{ stdenv, lib, makeWrapper, ghostscript, texlive, gnused, gnugrep, gawk
, coreutils, ... }:
stdenv.mkDerivation rec {
  name = "latexmk-yann";
  version = "4.82beta1yann";
  executable = ./latexmk-482beta1yann.pl;
  nativeBuildInputs = [ makeWrapper ]; # needed for wrapProgram below
  buildInputs =
    [ ghostscript texlive.combined.scheme-full gnused gnugrep gawk coreutils ];
  phases = [ "installPhase" ];
  installPhase = ''
    install -m755 ${executable} -D $out/bin/latexmk
    # create wrapper with proper paths for dependencies set
    wrapProgram $out/bin/latexmk --prefix PATH : ${lib.makeBinPath buildInputs}
  '';
}
