# Adapted from tomberek: https://github.com/NixOS/nixpkgs/issues/233835#issuecomment-1694602840
# Patch added from MarSoft: https://github.com/NixOS/nixpkgs/issues/233835#issuecomment-1742071337
{ pkgs ? import <nixpkgs> { }, ... }:
pkgs.rustPlatform.buildRustPackage rec {
  pname = "stl-thumb";
  version = "v0.5.0-dec70d8";

  # src = /home/tom/t7/stl-thumb;
  src = pkgs.fetchFromGitHub {
    owner = "unlimitedbacon";
    repo = pname;
    rev = "dec70d8d8cfbb6c635ecbd8c1f3d3959ea42f5e8";
    sha256 = "sha256-xF3sl3LOno3MeLK11cP48KlkbQDIZ/7Ax17dD8DL+po=";
  };

  doCheck = false;

  cargoHash = "sha256-7Nhnam1odL/Nk/oJjkj7CcqZXePpNi6VwJI1/J9Z1xM=";
  postInstall = ''
    mkdir $out/include
    cp libstl_thumb.h $out/include
    mkdir -p $out/share/thumbnailers
    mkdir -p $out/mime/packages
    cp stl-thumb.thumbnailer $out/share/thumbnailers/stl-thumb.thumbnailer
    cp stl-thumb-mime.xml $out/mime/packages/stl-thumb-mime.xml
  '';
  # libs are loaded dynamically; make make sure we'll find them
  # TODO: conditionally use wayland or xorg, not both!
  postFixup = with pkgs; ''
    patchelf \
      --add-needed ${xorg.libX11}/lib/libX11.so \
      --add-needed ${wayland}/lib/libwayland-client.so \
      --add-needed ${libGL}/lib/libEGL.so \
      $out/bin/stl-thumb
  '';
  meta = with pkgs.lib; {
    description = "Thumbnail generator for STL files";
    homepage = "https://github.com/unlimitedbacon/stl-thumb";
    license = licenses.mit;
    maintainers = [ ];
  };
  buildInputs = with pkgs; [ fontconfig xorg.libX11 ];
  propagatedBuildInputs = with pkgs.xorg; [ libXcursor libXrandr libXi ];
  nativeBuildInputs = with pkgs; [ cmake pkg-config ];
}
