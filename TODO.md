# ✅ TODOs

## ✨ Things to Try Out

- General
    - Don't pass `system` in specialArgs: https://isabelroses.com/blog/im-not-mad-im-disapointed-10
- 🏦 Uni
    - migrate broken gitlab-backup from beefy to here
- 🏡 HomeLab:
    - 🖼️ Immich
    - 📰 Paperless
        - auto-add files from e.g. Sync
    - 👀 Monit
    - 💽 Btrfs
        - snapshots
        - periodic auto-deduplication
    - 💽 Borg Backups
        - periodic borg copy from 4TB-SSD to 4TB-HDD
        - periodic borg copy from 4TB-SSD to hetzner
    - 🏠 HomeAssistant
- 💻 Ideapad
    - set up TLP with fine-tuned battery save settings

## 🐞 Bugs / Problems

...
