{ lib, ... }: {
  imports = [
    ./common.nix
    ./user.nix
    ./home-manager.nix
    ./lang.nix
    ./fhs.nix
    ./gnupg.nix
    ./ecryptfs.nix
    ./desktop
    ./collections
    ./services
    ./secrets
  ];
  yann.collections.core.enable = lib.mkDefault true;
}
