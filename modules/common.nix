{ config, pkgs, lib, ... }:
with lib; {
  nix = {
    package = pkgs.lix; # use lix instead of nix
    # regularly optimize the nix store (saves space with hard-linking)
    optimise.automatic = true;
    gc = {
      automatic = false;
      options = "--delete-older-than 30d";
    };
    settings = { experimental-features = [ "nix-command" "flakes" ]; };
  };
  nixpkgs.config.allowUnfree = mkDefault true;
  # Enable networking
  networking.networkmanager.enable = mkDefault true;
  networking.wireless.enable = mkDefault false;
  # Enable Docker
  # virtualisation.docker.enable = mkDefault true;
  virtualisation.podman = {
    enable = true;
    dockerCompat = true;
  };
  environment.systemPackages = with pkgs; [ ];
  # Compress part of RAM
  zramSwap.enable = lib.mkDefault true;
  # Be able to run ARM binaries
  boot.binfmt.emulatedSystems = mkDefault [ "aarch64-linux" "armv7l-linux" ];
  boot.supportedFilesystems = [ "ntfs" "btrfs" ];
  # cache executables in RAM for faster startup
  services.preload.enable = mkDefault true;
}
