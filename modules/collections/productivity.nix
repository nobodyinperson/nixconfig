{ pkgs, config, lib, ... }:
let cfg = config.yann.collections.productivity;
in with lib; {
  options.yann.collections.productivity = {
    enable = mkEnableOption "productivity tools";
  };
  config = mkIf cfg.enable {
    environment.systemPackages = with pkgs; [
      (pass.withExtensions (ext: with ext; [ pass-otp ]))
      opentimestamps-client
      timewarrior
      annextimelog
      hledger
      hledger-utils
    ];
  };
}
