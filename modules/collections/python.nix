{ pkgs, config, lib, ... }:
let cfg = config.yann.collections.python;
in with lib; {
  options.yann.collections.python = {
    enable = mkEnableOption "Python environment and tools";
    python = mkPackageOption pkgs "python3" { };
    packageSelectors = mkOption {
      type = with types; listOf (functionTo (listOf anything));
      default = [ ];
      description = "python packages selector functions";
      example = ''
        [
          (pypkgs: [ pypkgs.black ])
          (pypkgs: with pypkgs; [ jupyter nbstripout ])
        ]
      '';
    };
  };
  config = mkIf cfg.enable {
    environment.systemPackages = with pkgs;
      [
        (cfg.python.withPackages
          (pypkgs: unique (flatten (map (f: f pypkgs) cfg.packageSelectors))))
      ];
  };
}
