{ pkgs, config, lib, ... }:
let cfg = config.yann.collections.core;
in with lib; {
  options.yann.collections.core = {
    enable = mkEnableOption "core tools I want everywhere";
  };
  config = mkIf cfg.enable {
    # otherwise 'building man-cache' is very slow with fish
    documentation.man.generateCaches = false;
    programs = {
      fish.enable = true;
      vim.enable = true;
      vim.defaultEditor = true;
      vim.package = pkgs.vimHugeX;
    };
    environment.systemPackages = with pkgs; [
      bat
      moreutils # e.g. ts, tee
      num-utils # e.g. numsum
      pwgen
      curl
      borgbackup
      jq # JSON parsing
      vifm
      eza
      file
      btrfs-progs
      fish
      fzf
      htop
      btop # even fancier system monitor
      disko # 💽 partition disks declaratively
      gptfdisk
      git
      lsof
      watchexec
      lz4
      usbutils
      nmap
      dig
      magic-wormhole
      openssh
      openssl
      parallel
      starship
      tmux
      tree
      unzip
      wget
      xz
      zip
      zstd
      inxi
      expect # e.g. for unbuffer (faking interactive terminal to preserve color output)
      # fun stuff
      neofetch
      lolcat
      cowsay
      figlet
    ];
    # 🐚 SSH
    programs.ssh.knownHosts = {
      "github.com".publicKey =
        "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIOMqqnkVzrm0SdG6UOoqKLsabgH5C9okWi0dh2l9GKJl";
      "gitlab.com".publicKey =
        "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIAfuCHKVTjquxvt6CM6tdG4SLp1Btn/nOeHHE5UOzRdf";
      "umphylab.de".publicKey =
        "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIJAtCb5IiBe5pKOq/utmy+BoHM1if7jXNWmJ0i2LSJzS";
      "atris.fz-juelich.de".publicKey =
        "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIADN3q3SzHPsBjMg24xzmhEQoQNxA7K5r6mxVQBmrxgW";
      "nobodyinperson.mooo.com".publicKey =
        "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAILVNVespcXjqcK/KpBstjdT0VvO+4rqhE2dWcCqBHLVj";
    };
    environment.sessionVariables = mkMerge [
      (mkIf config.programs.vim.defaultEditor { SYSTEMD_EDITOR = "vim"; })
      # auto-run unfound commands
      (mkIf config.programs.command-not-found.enable {
        NIX_AUTO_RUN = "1";
        NIX_AUTO_RUN_INTERACTIVE = "1";
      })
    ];
  };
}
