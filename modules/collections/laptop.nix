{ pkgs, config, lib, ... }:
let cfg = config.yann.collections.laptop;
in with lib; {
  options.yann.collections.laptop = {
    enable = mkEnableOption "Laptop quirks";
  };
  config = mkIf cfg.enable {
    # touchpad
    services.libinput.touchpad.tappingDragLock = lib.mkDefault false;
    # power management
    powerManagement.enable = lib.mkDefault true;
    powerManagement.cpuFreqGovernor = lib.mkDefault "ondemand";
    powerManagement.powertop.enable = lib.mkDefault true;
  };
}
