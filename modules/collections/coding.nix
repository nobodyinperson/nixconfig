{ pkgs, config, lib, ... }:
let cfg = config.yann.collections.coding;
in with lib; {
  options.yann.collections.coding = { enable = mkEnableOption "coding tools"; };
  config = mkIf cfg.enable {
    yann.collections.python = {
      enable = mkDefault true;
      packageSelectors = mkDefault [
        (pypkgs:
          with pypkgs; [
            rich
            black
            isort
            jupyter
            jupyterlab
            jupyterlab-code-formatter
            nbconvert
            ipython
            textual
            ipympl
            tkinter
            poetry-core # for building Python packages
          ])
      ];
    };
    environment.systemPackages = with pkgs; [
      yanns-nix-scripts
      nixfmt-classic
      nixpkgs-fmt
      scons
      just
      gnumake
      shfmt
      binutils # strings etc...
      shellcheck
      clang-tools
      jq
      sqlite
      git
      git-annex
      difftastic
      reuse
      poetry
      pipx
      frogmouth # TUI markdown viewer
      # nbstripout # error: pytest-cram-0.2.2 not supported for interpreter python3.12
    ];
  };
}
