{ pkgs, config, lib, ... }:
let cfg = config.yann.collections.multimedia;
in with lib; {
  options.yann.collections.multimedia = {
    enable = mkEnableOption "multimedia tools";
  };
  config = mkIf cfg.enable {
    environment.systemPackages = with pkgs;
      mkMerge [
        [
          # media cli apps
          yt-dlp
          imagemagick
          ghostscript
          exiftool
          feh # --keep-zoom-vp skip through images with fixed zoom
          pdfminer
          diffpdf
          pandoc
          pdftk
          ffmpeg
          zbar
        ]
        (mkIf config.yann.desktop.enable [ # desktop apps
          pdfpc
          screenkey # keyboard overlay
          obs-studio
          libreoffice
          xournalpp
          cheese
          diffpdf
          inkscape
          vlc
          upscayl
          gimp
          evince
          simple-scan
          shotwell
          kdenlive
          highlight-pointer
        ])
      ];
  };
}
