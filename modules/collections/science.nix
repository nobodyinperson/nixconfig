{ pkgs, config, lib, ... }:
let cfg = config.yann.collections.science;
in with lib; {
  options.yann.collections.science = {
    enable = mkEnableOption "science tools";
  };
  config = mkIf cfg.enable {
    yann.collections.coding.enable = mkDefault true;
    yann.collections.python = {
      enable = mkDefault true;
      packageSelectors = mkDefault [
        (pypkgs:
          with pypkgs; [
            sympy
            numpy
            xarray
            netcdf4
            pandas
            matplotlib
            ipympl
            scipy
            tkinter
            pygobject3
            pint
            datalad-next
          ])
      ];
    };
    environment.systemPackages = with pkgs; [
      camp2ascii
      apptainer
      datalad
      kbibtex
      # qgis
      libqalculate
      texlive.combined.scheme-full
      netcdf # ncdump etc.
      gtk3 # needed for matplotlib?
      gobject-introspection # needed for matplotlib?
    ];
  };
}
