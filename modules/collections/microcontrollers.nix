{ pkgs, config, lib, ... }:
let cfg = config.yann.collections.microcontrollers;
in with lib; {
  options.yann.collections.microcontrollers = {
    enable = mkEnableOption "Utilities for embedded development";
  };
  config = mkIf cfg.enable {
    services.udev.packages = [ pkgs.platformio-core.udev ];
    environment.systemPackages = with pkgs; [
      platformio # programming microcontrollers
      mosquitto # mqtt utils
      binutils # 'strings' cmd etc.
      jq # JSON parsing
      # kicad
    ];
  };
}
