{ pkgs, config, lib, ... }:
let cfg = config.yann.collections.cad;
in with lib; {
  options.yann.collections.cad = { enable = mkEnableOption "CAD programs"; };
  config = mkIf cfg.enable {
    environment.systemPackages = (with pkgs; [
      stl-thumb # Adds STL thumbnailer
      f3d # 3d object thumbnailer
      openscad-unstable
      solvespace
      freecad
      prusa-slicer
    ]);
    yann.desktop.thumbnailers = {
      enable = mkDefault true;
      entries = {
        solvespace = {
          mimeTypes = [ "application/x-solvespace" ];
          cmd =
            "${pkgs.solvespace}/bin/solvespace-cli thumbnail --view isometric --size %sx%s  --output %o %i";
        };
        openscad = {
          mimeTypes = [ "application/x-openscad" ];
          cmd = "${pkgs.openscad}/bin/openscad -o %o --imgsize %s,%s %i";
        };
        "3mf" = {
          mimeTypes = [ "model/3mf" ];
          cmd = ''
            ${pkgs.bash}/bin/bash -c "${pkgs.unzip}/bin/unzip -p %i 'Metadata/thumbnail.png' > %o"'';
        };
        "gcode" = {
          mimeTypes = [ "text/x.gcode" ];
          cmd =
            "${pkgs.gcode-thumbnailer}/bin/gcode-thumbnailer %i --output %o";
        };
        "freecad" = {
          mimeTypes = [ "application/x-extension-fcstd" ];
          cmd = let
            freecad-thumbnailer =
              pkgs.writers.writePython3Bin "freecad-thumbnailer" {
                doCheck = false;
              } (builtins.readFile
                "${pkgs.freecad.src}/src/Tools/freecad-thumbnailer");
          in "${lib.getExe freecad-thumbnailer} -s %s %i %o";
        };
      };
    };
  };
}
