{ ... }: {
  imports = [
    ./core.nix
    ./cad.nix
    ./microcontrollers.nix
    ./coding.nix
    ./science.nix
    ./multimedia.nix
    ./productivity.nix
    ./laptop.nix
    ./python.nix
  ];
}
