# This script converts JSON to XML, mostly for Thunar's custom-actions format
import json
import xml.etree.ElementTree as ET
import argparse
import logging
import sys

logger = logging.getLogger(__name__)


parser = argparse.ArgumentParser(description="Convert JSON to XML")
parser.add_argument(
    "-i",
    "--input",
    type=argparse.FileType("r"),
    default=sys.stdin,
    help="Input JSON file (default: stdin)",
)
parser.add_argument(
    "-o",
    "--output",
    type=argparse.FileType("w"),
    default=sys.stdout,
    help="Output XML file (default: stdout)",
)
parser.add_argument(
    "-v",
    "--verbose",
    action="count",
    default=0,
    help="move -v → more debug output",
)

args = parser.parse_args()
logging.basicConfig(level=logging.WARNING - args.verbose * 10)

try:
    logger.info(f"Reading JSON from {args.input.name}")
    JSON = json.load(args.input)
except json.JSONDecodeError as e:
    parser.error(f"Couldn't parse JSON from {args.input.name}: {e!r}")

logger.info(
    f"JSON read from {args.input.name}:\n{json.dumps(JSON, indent=4, ensure_ascii=False)}"
)

match JSON:
    case dict():
        logger.debug("Given JSON is a dict ✅")
    case _:
        parser.error(
            f"Input {JSON = } from {args.input.name} is not a dict, but {type(JSON)}!"
        )

if (N := len(JSON)) != 1:
    parser.error(
        f"Input {JSON = } has not exactly one element but {N}. "
        f"You can specify the --root argument to wrap everything in a specific element."
    )


root = ET.Element(name := list(JSON.keys())[0])
JSON = JSON.pop(name)


def xml_to_str(node):
    return ET.tostring(node, encoding="utf-8", xml_declaration=True).decode()


def add_to_xml(node, obj):
    logger.debug(f"Will add {obj = !r} to {node = !r}")
    match obj:
        case bool() if obj:
            pass  # if True, no need to do anything (element is already there)
        case list() as L:
            for e in L:
                add_to_xml(node, e)
        case dict() as d:
            for k, v in d.items():
                if v:  # only add new element if there is something to be put inside
                    subnode = ET.SubElement(node, k)
                    add_to_xml(subnode, v)
        case _:  # anything else just gets stringified
            node.text = str(obj)


add_to_xml(root, JSON)  # Add the JSON recursively into the XML root element
ET.indent(root)  # pretty-format in-place
XMLstr = xml_to_str(root)  # turn XML to string
args.output.write(XMLstr)  # write to output
