{ pkgs, config, lib, ... }@args:
with lib;
let cfg = config.programs.thunar.customActions;
in {
  options.programs.thunar.customActions = {
    enable = mkEnableOption "declarative Thunar custom actions";
    actions = mkOption {
      type = types.listOf (types.submodule {
        options = {
          name = mkOption {
            type = types.str;
            description = "name of the action in the context menu";
          };
          icon = mkOption {
            type = types.str;
            description =
              "name of the icon (browse e.g. with `nix run nixpkgs#pantheon.elementary-iconbrowser`)";
            example = "utilities-terminal";
          };
          submenu = mkOption {
            type = with types; nullOr str;
            description =
              "If specified, the action will be put into a context submenu with this name. Use this to group actions.";
            default = null;
          };
          command = mkOption {
            type = with types; nullOr str;
            description = ''
              Command to be run. The following placeholders are available:
              %f path to first selected file/folder
              %F paths to all selected files/folders
              %d folder of first selected file (from %f)
              %D folder of all selected files (from %F)
              %n first selected filename (without path)
              %N all selected filenames (without path)
            '';
            default = null;
          };
          description = mkOption {
            type = with types; nullOr str;
            description =
              "Description shown in Thunar's status bar when hovering over the action.";
            default = null;
          };
          range = mkOption {
            type = with types; nullOr str;
            description =
              "Limit how many files/folders this action may be applied to. E.g. `1` means only one element, `1-4` means up to four, etc.";
            default = null;
          };
          patterns = mkOption {
            type = with types; listOf str;
            description =
              "glob patterns this action can be applied on. E.g. `*.pdf` would only show this actions for PDF files.";
            default = [ ];
            apply = concatStringsSep ";";
            example = ''
              ["*.png" "*.jpg" "*.tiff"] # certain image files
              ["*.pdf" "*.ods" "*.odt"]  # document files
              ["*.gpg" "*.asc" ]         # GnuPG-encrypted files
            '';
          };
          directories = mkOption {
            type = types.bool;
            description = "whether this action should be shown for directories";
            default = false;
          };
          audio-files = mkOption {
            type = types.bool;
            description = "whether this action should be shown for audio files";
            default = false;
          };
          image-files = mkOption {
            type = types.bool;
            description = "whether this action should be shown for image files";
            default = false;
          };
          text-files = mkOption {
            type = types.bool;
            description = "whether this action should be shown for text files";
            default = false;
          };
          video-files = mkOption {
            type = types.bool;
            description = "whether this action should be shown for video files";
            default = false;
          };
          other-files = mkOption {
            type = types.bool;
            description = "whether this action should be shown for other files";
            default = false;
          };
        };
      });
    };
  };
  config = let
    json2xml = pkgs.writers.writePython3Bin "json2xml" {
      flakeIgnore = [ "E202" "E251" "E501" ];
    } ./json2xml.py;
    customActionsJSON = { "actions" = map (a: { "action" = a; }) cfg.actions; };
    customActionsFile = pkgs.runCommand "Thunar-uca.xml" { } ''
      ${getExe json2xml} -o $out <<'EOT'
      ${builtins.toJSON customActionsJSON}
      EOT
    '';
  in mkIf cfg.enable {
    home.file.".config/Thunar/uca.xml" = {
      source = customActionsFile;
      force = true;
    };
  };
}
