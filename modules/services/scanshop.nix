{ pkgs, config, lib, ... }:
with lib;
let cfg = config.yann.services.scanshop;
in {
  options.yann.services.scanshop = {
    enable = mkEnableOption
      "ScanShop, scan products to add them to your shopping list";
    package = mkPackageOption pkgs "scanshop" { };
    flaskList = mkOption {
      type = with types; nullOr str;
      default = null;
      description =
        "Flask.io list id to modify. Note that specifying this option will put the list id into the nix store.";
      example = "BqDQJ4uLpU9c";
    };
    device = mkOption {
      type = with types; nullOr str;
      default = null;
      description = "barcode scanner to use, --device flag";
      example = ''"/dev/input/event10" or "usb.*keyboard"'';
    };
    cacheFile = mkOption {
      type = with types; nullOr path;
      description =
        "product cache file. Specify `null` to use in-memory caching.";
      default = "/var/lib/scanshop/products.json";
    };
    extraFlags = mkOption {
      type = with types; listOf str;
      description = "list of extra flags passed to the scanshop command";
      default = [ ];
      example = ''[ "-vvv" ]'';
    };
    environmentFile = mkOption {
      type = with types; nullOr path;
      default = null;
      description = "An environment file as defined in systemd.exec(5)";
      example = ''
        e.g. "/etc/scanshop.env" with content:
        ```
        FLASKIO_LIST="BqDQJ4uLpU9c"
        OPENGTINDB_USERID="854239672813021675053"
        ```
      '';
    };
  };
  config = mkIf cfg.enable {
    environment.systemPackages = [ cfg.package ];
    # TODO: 🛡️ hardening!
    systemd.services.scanshop = {
      description =
        "ScanShop - scan products to add them to your shopping list";
      wantedBy = [ "multi-user.target" ];
      serviceConfig = {
        ExecStart = let
          launcher = pkgs.writeShellApplication {
            name = "scanshop-launcher";
            runtimeInputs = [ cfg.package ];
            text = ''
              read-barcodes --grab ${
                optionalString (cfg.device != null)
                "--device ${escapeShellArg cfg.device}"
              } | scanshop ${
                optionalString (cfg.flaskList != null)
                "--FlaskIO-list ${escapeShellArg cfg.flaskList}"
              } ${
                optionalString (cfg.cacheFile != null)
                "--cache ${escapeShellArg cfg.cacheFile}"
              } ${concatStringsSep " " (map escapeShellArg cfg.extraFlags)}
            '';
          };
        in "${getExe launcher}";
        Restart = "always";
        RestartSec = "5";
        EnvironmentFile =
          lib.optional (cfg.environmentFile != null) cfg.environmentFile;
      };
    };
    assertions = [{
      assertion = !(isNull cfg.environmentFile && isNull cfg.flaskList);
      message =
        "You need to specify a Flask.io list either via `yann.services.scanshop.flaskList` or in a `scanshop.environmentFile`";
    }];
  };
}
