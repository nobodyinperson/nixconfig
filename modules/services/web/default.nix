{ config, lib, ... }:
with lib;
let cfg = config.yann.services.web;
in {
  imports = [
    ./server.nix
    ./forgejo
    ./jupyterhub.nix
    ./homebox.nix
    ./paperless.nix
    ./freedns.nix
    ./netcup-setdns.nix
  ];
  options = {
    # TODO: Make this take multiple domains
    # A host might be reachable via multiple domains (multiple DynDNS providers, etc.)
    yann.services.web.domain = mkOption {
      type = types.str;
      description = "Public domain where this web server is reachable";
      default = config.networking.fqdn;
      example = "example.com";
    };
  };
}
