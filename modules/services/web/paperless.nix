{ pkgs, config, lib, ... }:
let cfg = config.yann.services.web.paperless;
in with lib; {
  options.yann.services.web.paperless = {
    enable = mkEnableOption "paperless document management";
    public = mkEnableOption "make paperless publicly accessible";
    port = mkOption {
      description = "local paperless port";
      type = types.port;
      default = 28981;
    };
    publicPort = mkOption {
      description =
        "public-facing port (paperless can't run under subpath) for when public=true";
      type = types.port;
      default = cfg.port + 1;
    };
    moduleConfig = mkOption {
      description = "extra module config";
      type = types.attrs;
      default = { };
    };
    backup = mkOption {
      description = "automatic data backup";
      type = types.submodule {
        options = {
          enable = mkEnableOption "automatic data backup";
          RequiresMountsFor = mkOption {
            description = "set this to the mountpoint of the backup drive";
            type = types.path;
            default = cfg.backup.to;
          };
          to = mkOption {
            description = "path to borg repo to back up to";
            type = types.path;
          };
        };
      };
      default = { };
    };
  };
  config = mkIf cfg.enable (mkMerge [
    # common config if paperless is enabled
    { services.paperless.enable = true; }
    {
      services.paperless = cfg.moduleConfig;
    }
    # if paperless should be publicly available
    (mkIf cfg.public {
      services.paperless.settings = {
        PAPERLESS_URL = "https://${config.yann.services.web.domain}:${
            toString cfg.publicPort
          }";
      };
      # Proxy from outside to local port
      # paperless can't run under a subpath, and we can't do subdomains, so we need to use a port
      services.nginx = {
        # another virtual host
        virtualHosts."${config.yann.services.web.domain}-paperless" = let
          sslDir = # the common ssl certificates
            config.security.acme.certs."${config.yann.services.web.domain}".directory;
        in {
          serverName = config.yann.services.web.domain;
          forceSSL = true;
          # use the common ssl certs
          sslCertificate = "${sslDir}/fullchain.pem";
          sslCertificateKey = "${sslDir}/key.pem";
          listen = [{ # listen on the public paperless port and to ssl
            port = cfg.publicPort;
            addr = "0.0.0.0";
            ssl = true;
          }];
          locations."/" = {
            proxyPass = "http://localhost:${toString cfg.port}";
            proxyWebsockets = true;
          };
        };
      };
      networking.firewall.allowedTCPPorts = [ cfg.publicPort ];
      # Put link on homepage
      yann.services.web.server = {
        enable = mkDefault true;
        homepage.enable = mkDefault true;
        homepage.links."📝 Paperless (Document Management)" =
          "https://${config.yann.services.web.domain}:${
            toString cfg.publicPort
          }";
      };
    })
    # if backups are enabled
    (mkIf cfg.backup.enable {
      # Back up forgejo data regularly
      services.borgbackup.jobs.paperless = {
        paths = config.services.paperless.dataDir;
        repo = cfg.backup.to;
        encryption.mode = "none";
        compression = "auto,zstd";
        inhibitsSleep = true;
        # extraCreateArgs = [ "--progress" "--stats" ]; # TODO: currently broken, fix here: https://github.com/NixOS/nixpkgs/pull/332319
        startAt = "daily";
      };
      systemd.services.borgbackup-job-paperless = rec {
        unitConfig.RequiresMountsFor = cfg.backup.RequiresMountsFor;
        # pause paperless during backup, see https://github.com/systemd/systemd/issues/34954
        conflicts = map (s: "paperless-${s}.service") [
          # stop paperless components during backup
          "web"
          "scheduler"
          "task-queue"
          "consumer"
        ] ++ [
          # for some reason nginx needs to be restarted
          # for the paperless webinterface not give '502 bad gateway' after the paperless restart.
          # TODO: Ideally nginx is only restarted *after* the backup, not stopped *during* the backup
          "nginx.service"
        ];
        after = conflicts; # start after paperless is fully down
        onSuccess = conflicts; # start paperless again afterwards
        onFailure = conflicts; # start paperless again afterwards
      };
      systemd.timers."borgbackup-job-paperless".timerConfig.RandomizedDelaySec =
        "1h";
    })
  ]);
}
