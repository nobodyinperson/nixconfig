{ pkgs, config, lib, ... }:
with lib;
let cfg = config.yann.services.web.server;
in {
  options.yann.services.web.server = {
    enable = mkEnableOption "web server";
    public = mkEnableOption "web server accessible publicly";
    homepage = mkOption {
      description = "auto-generated homepage";
      type = types.submodule {
        options = {
          enable = mkEnableOption "auto-generated homepage";
          title = mkOption {
            type = types.str;
            description = "home page title";
            default = "Yann's Web Services";
          };
          prolog = mkOption {
            type = with types; listOf str;
            description = "HTML bits to display at the top";
            default = [ ];
          };
          epilog = mkOption {
            type = with types; listOf str;
            description = "HTML bits to display at the end";
            default = [ ];
          };
          links = mkOption {
            type = with types; attrsOf str;
            default = { };
            description = "mapping of link text to link target";
            example = ''
              {
                "🐍 Jupyter": "/jupyter";
                "🔨 Forgejo": "/forgejo";
              }
            '';
          };
        };
      };
    };
  };
  config = mkIf cfg.enable {
    # opening to the outside
    networking.firewall.allowedTCPPorts = mkMerge [
      (mkIf cfg.public [ 443 ]) # HTTPS port if public
      # Port 80 needed for standard Let's Encrypt
      (mkIf (!(config.security.acme.defaults.tlsMode or false)) [ 80 ])
    ];
    # 🔐 SSL encryption with proper LetsEncrypt certificates
    security.acme = {
      acceptTerms = true;
      defaults = mkMerge [{
        email = mkDefault "nobodyinperson@posteo.de";
        # test server for debugging purposes
        # server = "https://acme-staging-v02.api.letsencrypt.org/directory";
      }
      # I introduced security.acme.defaults.{tlsMode,conflictingServices} in nixpkgs PR https://github.com/NixOS/nixpkgs/pull/340136, which is not yet merged.
      # TODO: the below gives infinite recursion.
      # Would be great if nginx could automatically be added as conflict
      # (mkIf (config.security.acme.defaults.tlsMode or false) {
      #   conflictingServices = mkDefault [ "nginx.service" ];
      # })
        ];
    };
    warnings = mkIf ((config.security.acme.defaults.tlsMode or false)
      && !(elem "nginx.service"
        config.security.acme.defaults.conflictingServices)) [''
          security.acme.defaults.tlsMode is true but nginx.service is not in security.acme.defaults.conflictingServices, which will result in certificate acquiry failure. Due to an infinite recursion error this currently can't be done automatically, so fix it with:
          security.acme.defaults.conflictingServices = [ "nginx.service" ];
        ''];
    services.nginx = {
      enable = true;
      recommendedGzipSettings = true;
      recommendedOptimisation = true;
      recommendedProxySettings = true;
      recommendedTlsSettings = true;
      virtualHosts."${config.yann.services.web.domain}" = {
        forceSSL = true;
        enableACME = true; # LetsEncrypt Integration
        root = mkIf cfg.homepage.enable (let
          links =
            mapAttrsToList (text: target: ''<a href="${target}">${text}</a>'')
            cfg.homepage.links;
          linkSection =
            concatMapStringsSep "\n" (link: "<h2>${link}</h2>") links;
          prologs = concatMapStringsSep "\n" (content: ''
            <p>
            ${content}
            </p>'') cfg.homepage.prolog;
          epilogs = concatMapStringsSep "\n" (content: ''
            <p>
            ${content}
            </p>'') cfg.homepage.epilog;
          homepage = pkgs.writeTextFile {
            name = "index.html";
            text = ''
              <html>
              <head>
                <meta charset="UTF-8">
                <title>${cfg.homepage.title}</title>
                <style>
                    body { font-family: sans-serif; }
                </style>
              </head>
              <body>
                <center>
                  <h1>${cfg.homepage.title}</h1>
                  ${prologs}
                  ${linkSection}
                  ${epilogs}
                </center>
              </body>
              </html>
            '';
            destination = "/www/index.html";
          };
        in "${homepage}/www");
        locations."/" = { };
      };
    };
  };
}
