{ pkgs, config, lib, ... }:
let cfg = config.yann.services.web.homebox;
in with lib; {
  options.yann.services.web.homebox = {
    enable = mkEnableOption "homebox inventory management";
    public = mkEnableOption "make homebox publicly accessible";
    dataDir = mkOption {
      description = "path to data directory";
      type = types.path;
      default = "/var/lib/homebox/data";
    };
    port = mkOption {
      description = "local homebox port";
      type = types.port;
      default = 7745;
    };
    publicPort = mkOption {
      description =
        "public-facing port (HomeBox can't run under subpath) for when public=true";
      type = types.port;
      default = cfg.port + 1;
    };
    settings = mkOption {
      description = "extra settings";
      type = with types; attrsOf str;
      default = { };
    };
    backup = mkOption {
      description = "automatic data backup";
      type = types.submodule {
        options = {
          enable = mkEnableOption "automatic data backup";
          RequiresMountsFor = mkOption {
            description = "set this to the mountpoint of the backup drive";
            type = types.path;
            default = cfg.backup.to;
          };
          to = mkOption {
            description = "path to borg repo to back up to";
            type = types.path;
          };
        };
      };
      default = { };
    };
  };
  config = mkIf cfg.enable (mkMerge [
    # common config if homebox is enabled
    {
      services.homebox = {
        enable = true;
        package = mkDefault pkgs.homebox;
        settings = mkMerge [
          rec {
            HBOX_STORAGE_DATA = cfg.dataDir;
            HBOX_STORAGE_SQLITE_URL =
              "${cfg.dataDir}/homebox.db?_pragma=busy_timeout=999&_pragma=journal_mode=WAL&_fk=1";
            HBOX_WEB_PORT = toString cfg.port;
            HBOX_OPTIONS_ALLOW_REGISTRATION = "false";
            HBOX_MODE = "production";
          }
          cfg.settings
        ];
      };
      # some fixes the NixOS homebox module doesn't provide
      systemd.services.homebox = {
        serviceConfig.WorkingDirectory = mkForce cfg.dataDir;
        serviceConfig.ReadWritePaths = mkForce [ cfg.dataDir ];
        unitConfig.RequiresMountsFor = mkForce cfg.dataDir;
      };
    }
    # if HomeBox should be publicly available
    (mkIf cfg.public {
      # Proxy from outside to local port
      # HomeBox can't run under a subpath, and we can't do subdomains, so we need to use a port
      services.nginx = {
        # another virtual host
        virtualHosts."${config.yann.services.web.domain}-homebox" = let
          sslDir = # the common ssl certificates
            config.security.acme.certs."${config.yann.services.web.domain}".directory;
        in {
          serverName = config.yann.services.web.domain;
          forceSSL = true;
          # use the common ssl certs
          sslCertificate = "${sslDir}/fullchain.pem";
          sslCertificateKey = "${sslDir}/key.pem";
          listen = [{ # listen on the public homebox port and to ssl
            port = cfg.publicPort;
            addr = "0.0.0.0";
            ssl = true;
          }];
          locations."/" = {
            proxyPass = "http://localhost:${toString cfg.port}";
            proxyWebsockets = true;
          };
        };
      };
      networking.firewall.allowedTCPPorts = [ cfg.publicPort ];
      # Put link on homepage
      yann.services.web.server = {
        enable = mkDefault true;
        homepage.enable = mkDefault true;
        homepage.links."📦 HomeBox (Inventory)" =
          "https://${config.yann.services.web.domain}:${toString cfg.publicPort}";
      };
    })
    # if backups are enabled
    (mkIf cfg.backup.enable {
      # Back up forgejo data regularly
      services.borgbackup.jobs.homebox = {
        paths = cfg.dataDir;
        repo = cfg.backup.to;
        encryption.mode = "none";
        compression = "auto,zstd";
        inhibitsSleep = true;
        # extraCreateArgs = [ "--progress" "--stats" ]; # TODO: currently broken, fix here: https://github.com/NixOS/nixpkgs/pull/332319
        startAt = "daily";
      };
      systemd.services.borgbackup-job-homebox = rec {
        unitConfig.RequiresMountsFor = cfg.backup.RequiresMountsFor;
        # pause homebox during backup, see https://github.com/systemd/systemd/issues/34954
        conflicts = [ "homebox.service" ]; # stop homebox during backup
        after = conflicts; # start after homebox is fully down
        onSuccess = conflicts; # start homebox again afterwards
        onFailure = conflicts; # start homebox again afterwards
      };
      systemd.timers."borgbackup-job-homebox".timerConfig.RandomizedDelaySec =
        "1h";
    })
  ]);
}
