{ pkgs, config, lib, ... }:
with lib;
let cfg = config.yann.services.web.freedns;
in {
  options.yann.services.web.freedns = {
    enable = mkEnableOption "FreeDNS.afraid.org IP updater";
    interval = mkOption {
      type = types.str;
      description =
        "Systemd calendar expression for contacting FreeDNS to do an IP update";
      default = "daily";
      example = "*:0/15";
    };
    token = mkOption {
      type = with types; nullOr str;
      default = null;
      description = ''
        The token associated with your subdomain on FreeDNS.afraid.org.
        You can get it [here](https://freedns.afraid.org/dynamic/)
        and extract it from any of the example scripts.
        Be aware that specifying this option will put the token into the world-readable nix store and it's better to use `tokenFile`.
      '';
      example = "8PvK9BQqYwL4X6tGZM7cN8r2d3H6sF1eJk8UyRnT5pGu";
    };
    tokenFile = mkOption {
      type = with types; nullOr str;
      default = null;
      description =
        "Path during runtime containing the FreeDNS token. Overwrites `token`.";
      example = "/tmp/token.txt";
    };
  };
  config = mkIf cfg.enable {
    assertions = [{
      assertion = isNull cfg.token != isNull cfg.tokenFile;
      message = "need to specify exactly one of yann.services.web.freedns.{token,tokenFile}";
    }];
    systemd.services.freedns = {
      startAt = cfg.interval;
      script = let
        freedns-updater = pkgs.writeShellApplication {
          name = "freedns-updater";
          runtimeInputs = with pkgs; [ curl ];
          text = ''
            # token specified directly in config / nix store
            ${toShellVar "TOKEN" (cfg.token or null)}
            # token specified in external file
            ${toShellVar "TOKENFILE" (cfg.tokenFile or null)}
            if test -n "$TOKENFILE";then
              TOKEN="$(cat "$TOKENFILE" || true)"
            fi
            if test -z "$TOKEN";then
              echo >&2 "No token found!"
              exit 1
            fi
            curl -s "https://freedns.afraid.org/dynamic/update.php?$TOKEN"
          '';
        };
      in "${getExe freedns-updater}";
    };
    systemd.timers.freedns.timerConfig.RandomizedDelaySec = 30;
  };
}
