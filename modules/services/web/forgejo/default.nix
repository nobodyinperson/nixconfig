{ pkgs, config, lib, system, ... }:
### ⬆️  Update Instructions
# - empty-out all `sha256-...` hashes in this file
# - find the current forgejo-aneksajo version (like v9.0.3-git-annex1`) here: https://codeberg.org/forgejo-aneksajo/forgejo-aneksajo/tags
# - find the matching nixpkgs commit for the matching forgejo base Version - if it doesn't exist, go bullshit might be incoming
# - (Yann: rebase your custom branch onto that)
# - update the corresponding commit and baseVersion below
# - then nixos-rebuild and enter the hashes until it builds
with lib;
let
  cfg = config.yann.services.web.forgejo;
  fcfg = config.services.forgejo;
  # NixOS commit 'forgejo: 10.0.0 -> 10.0.1'
  # TODO: Pin this in the flake? Put this module into its own flake?
  nixpkgs-forgejo = import (fetchTarball {
    url =
      "https://github.com/NixOS/nixpkgs/archive/523ef8a9d30170f382eb0ef1e93baa9d7a94ff42.tar.gz";
    sha256 = "sha256:112s2f5vv1b7bmdv9m5l4p5zmj0kqjdw78jb5yc7ir5rvwk0rs25";
  }) {
    inherit system; # fix `error: attribute 'currentSystem' missing` in flakes
  };
  forgejo-aneksajo = nixpkgs-forgejo.forgejo.overrideAttrs (oldAttrs:
    let
      forgejo-src-nixpkgs = "${nixpkgs-forgejo.path}/pkgs/by-name/fo/forgejo";
      baseVersion = "10.0.1-git-annex1";
      src = pkgs.fetchFromGitea {
        domain = "codeberg.org";
        owner = "nobodyinperson";
        repo = "forgejo-aneksajo";
        # cherry-picked compound extensions and single-diff-link onto matrss' v10.0.1-git-annex1
        rev = "f3f44909cd7adb9a4d31d982f51508d6bba80e61";
        hash = "sha256-GKV58I0k72KHNxpQdH21yhFPoeaWkSrM0S6+x/URw3c=";
      };
      frontend = pkgs.buildNpmPackage {
        pname = "forgejo-frontend";
        inherit src;
        npmDepsHash = "sha256-EilG3wNu5153xRXiIhgQaxe3sh1TnSlMPQPUhqSh9mM=";
        version = "v${baseVersion}-${src.rev}";
        patches =
          [ "${forgejo-src-nixpkgs}/package-json-npm-build-frontend.patch" ];
        installPhase = ''
          mkdir $out
          cp -R ./public $out/
        '';
      };
    in {
      inherit src;
      doCheck = false; # tests just take too long...
      version = assert lib.asserts.assertMsg
        (lib.strings.hasPrefix oldAttrs.version baseVersion) ''
          You're trying to override forgejo v${oldAttrs.version} with v${baseVersion}.
                    That will most likely not work.
                    Update your nixpkgs used for overriding forgejo or adjust the forgejo source rev accordingly.'';
        "v${baseVersion}-${src.rev}";
      vendorHash = "sha256-fqHwqpIetX2jTAWAonRWqF1tArL7Ik/XXURY51jGOn0=";
      passthru = { inherit (frontend) npmDeps; }; # needed ?
      # copied from nixpkgs forgejo because we need copy the frontent files as well
      postInstall = ''
        mkdir $data
        cp -R ./{templates,options} ${frontend}/public $data
        mkdir -p $out
        cp -R ./options/locale $out/locale
        wrapProgram $out/bin/gitea \
          --prefix PATH : ${
            lib.makeBinPath (with pkgs; [ bash git gzip openssh ])
          }
      '';
    });
  # 🔧 Utilities
  # beef up an external renderer section
  # (last argument to mapAttrs' left out so it's a function that takes an attrset)
  sections-from-renderer = name: renderer:
    ({
      "markup.${name}" =
        # default values
        {
          ENABLED = true;
        } //
        # given values but without our internal attributes
        (builtins.removeAttrs renderer [
          "runtimeInputs"
          "monospace"
          "sanitizers"
        ]) //
        # overwrite certain values with intelligent value conversion
        rec {
          FILE_EXTENSIONS = let exts = renderer.FILE_EXTENSIONS or ".${name}";
          in (if builtins.isString exts then
            exts
          else
            concatStringsSep "," exts);
          IS_INPUT_FILE = renderer.IS_INPUT_FILE or (strings.hasInfix "$1"
            (renderer.RENDER_COMMAND or ""));
          RENDER_COMMAND = let
            cmd = pkgs.writeShellApplication {
              name = "render-${name}";
              runtimeInputs = renderer.runtimeInputs or [ ];
              text = (optionalString IS_INPUT_FILE ''
                if ! test -e "$1" || (head -n1 "$1" | grep -q "/annex/");then
                  echo '<i>It seems this file is not present.</i>'
                  exit
                fi
              '') + ''
                echo '<h3><code>${name}</code> Preview 👀</h3>'
              '' + (lib.optionalString (renderer.monospace or true) ''
                echo '<pre>'
              '') + ''
                if ! (${renderer.RENDER_COMMAND or ""});then
                  echo "$0 $*" "failed (exit code $?)"
                fi
              '' + (lib.optionalString (renderer.monospace or true) ''
                echo '</pre>'
              '');
            };
            timeout-command = pkgs.writeShellApplication {
              name = "render-${name}-timeout";
              runtimeInputs = with pkgs; [ coreutils ];
              text = let timeout = toString (renderer.timeout or 30);
              in ''
                if ! (timeout --verbose --kill-after=10  ${timeout} ${
                  getExe cmd
                } "$@" 2>&1);then
                  echo '${name} renderer failed or took too long.'
                fi
              '';
            };
          in "${getExe timeout-command}";
        };
    }) // # now add the sanitizers (if any)
    (mapAttrs' (sname: sanitizer:
      nameValuePair "markup.sanitizer.${name}.${sname}" sanitizer)
      (renderer.sanitizers or { }));
  make-renderer-sections = renderers:
    mergeAttrsList (mapAttrsToList sections-from-renderer renderers);
in {
  options.yann.services.web.forgejo = {
    enable = mkEnableOption "Forgejo with git-annex support";
    public = mkEnableOption "make forgejo publicly accessible";
    port = mkOption {
      description = "local forgejo port";
      type = types.port;
      default = 3000;
    };
    subpath = mkOption {
      description = "subpath for forgejo";
      type = with types; nullOr str;
      default = "forgejo";
    };
    stateDir = mkOption {
      description = "where forgejo should store all its data";
      type = types.path;
    };
    templateDir = mkOption {
      description = "folder of custom templates to use";
      type = with types; nullOr path;
      default = null;
    };
    logo = mkOption {
      description = "custom logo";
      type = with types; nullOr path;
      default = null;
    };
    backup = mkOption {
      description = "automatic data backup";
      type = types.submodule {
        options = {
          enable = mkEnableOption "automatic data backup";
          RequiresMountsFor = mkOption {
            description = "set this to the mountpoint of the backup drive";
            type = types.path;
            default = cfg.backup.to;
          };
          to = mkOption {
            description = "path to borg repo to back up to";
            type = types.path;
          };
        };
      };
      default = { };
    };
  };
  config = mkIf cfg.enable (mkMerge [
    {
      nixpkgs.overlays = [ (final: prev: { inherit forgejo-aneksajo; }) ];
      # Give forgejo access to git-annex
      systemd.services."forgejo".path = with pkgs; [ git-annex ];
      # Run forgejo
      services.forgejo = {
        enable = true;
        package = pkgs.forgejo-aneksajo;
        stateDir = cfg.stateDir;
        settings = {
          DEFAULT = {
            APP_NAME = "Yann's Forgejo Instance";
            # RUN_MODE = "dev";
          };
          log.LEVEL = "Warn";
          service = {
            REQUIRE_SIGNIN_VIEW = true; # must be signed in to see anything
            DISABLE_REGISTRATION = true; # only admin can make accounts
          };
          "ssh.minimum_key_sizes".RSA = 2048; # compat with Ubuntu ssh keys
          server = {
            DOMAIN = if cfg.public then
              config.yann.services.web.domain
            else
              "localhost";
            ROOT_URL = "${if cfg.public then "https" else "http"}://%(DOMAIN)s"
              + (optionalString (cfg.subpath != null) "/${cfg.subpath}");
            HTTP_PORT = cfg.port;
            SSH_PORT = builtins.head config.services.openssh.ports;
          };
          ui.MAX_DISPLAY_FILE_SIZE = 5000000000; # 5GB
          repository = {
            ENABLE_PUSH_CREATE_USER = true;
            ENABLE_PUSH_CREATE_ORG = true;
          };
          annex.ENABLED = true;
        } // (let
          headmiddletail = pkgs.writeShellApplication {
            name = "headmiddletail";
            runtimeInputs = [ pkgs.gawk ];
            text = builtins.readFile ./headmiddletail.sh;
          };
        in make-renderer-sections {
          "log.gz" = {
            FILE_EXTENSIONS = map (x: ".${x}.gz") [ "csv" "dat" "log" ];
            runtimeInputs = [ headmiddletail pkgs.gzip ];
            RENDER_COMMAND = ''zcat "$1" | headmiddletail'';
          };
          "log.zst" = {
            FILE_EXTENSIONS = map (x: ".${x}.zst") [ "csv" "dat" "log" ];
            runtimeInputs = [ headmiddletail pkgs.zstd ];
            RENDER_COMMAND = ''zstdcat "$1" | headmiddletail'';
          };
          "log.xz" = {
            FILE_EXTENSIONS = map (x: ".${x}.xz") [ "csv" "dat" "log" ];
            runtimeInputs = [ headmiddletail pkgs.xz ];
            RENDER_COMMAND = ''xzcat "$1" | headmiddletail'';
          };
          "zip" = {
            runtimeInputs = with pkgs; [ unzip ];
            RENDER_COMMAND = ''unzip -l "$1"'';
          };
          "pandoc" = {
            monospace = false;
            timeout = 10;
            runtimeInputs = with pkgs; [ pandoc ];
            FILE_EXTENSIONS = [ ".doc" ".docx" ".odt" ".ods" ];
            NEED_POSTPROCESS = false;
            # ⚠️  Disabling sanitization is unsafe!
            # But it is fine for now, because this instance is only used internally.
            RENDER_CONTENT_MODE = "no-sanitizer";
            RENDER_COMMAND = ''pandoc -t html --embed-resources "$1"'';
          };
          "tar" = {
            FILE_EXTENSIONS =
              map (x: ".tar${x}") [ "" ".gz" ".xz" ".bz2" ".zst" ];
            runtimeInputs = with pkgs; [ gnutar xz zstd bzip2 ];
            RENDER_COMMAND = ''tar -tvf "$1"'';
          };
          "HTML" = {
            monospace = false;
            timeout = 10;
            FILE_EXTENSIONS = [ ".html" ".htm" ];
            RENDER_COMMAND = "cat";
          };
          "NetCDF" = {
            FILE_EXTENSIONS = [ ".nc" ".znc" ];
            runtimeInputs = with pkgs; [ netcdf ];
            RENDER_COMMAND = ''ncdump -h "$1"'';
          };
          # TODO: This doesn't belong here, rather into an option extraRenderers
          "PARASITE-keras-model" = {
            FILE_EXTENSIONS = [ ".parasite.keras" ];
            runtimeInputs = with pkgs; [ netcdf ];
            RENDER_COMMAND = let
              # Pyhton 3.11 (not 3.12) because: error: tensorflow-2.13.0 not supported for interpreter python3.12
              # TODO: This is a mess, can't this be prettier?
              renderer = pkgs.writers.makePythonWriter pkgs.python311
                pkgs.python311Packages pkgs.buildPackages.python311Packages
                "/bin/parasite-model-summary" {
                  doCheck = false;
                  libraries = with pkgs.python311Packages; [ keras numpy ];
                } (builtins.readFile ./parasite.keras-renderer.py);
            in ''${getExe renderer} "$1"'';
          };
          "Jupyter-Notebook" = {
            # TODO: styling, syntax highlighting, math, ... But images work already!
            FILE_EXTENSIONS = ".ipynb";
            monospace = false;
            runtimeInputs = with pkgs;
              [ (python3.withPackages (ps: with ps; [ jupyter nbconvert ])) ];
            RENDER_COMMAND =
              "jupyter nbconvert --stdin --stdout --to html --template classic";
            NEED_POSTPROCESS = false;
            # ⚠️  Disabling sanitization is unsafe!
            # But it is fine for now, because this instance is only used internally.
            # This at least gets syntax highlighting of code cells working.
            # Still, Math/LaTeX in markdown or output cells is still broken (only $\displaystyle... shown)
            RENDER_CONTENT_MODE = "no-sanitizer";
            sanitizers = {
              "img".ALLOW_DATA_URI_IMAGES =
                true; # make images in notebooks work
              "label-for" = {
                ELEMENT = "label";
                ALLOW_ATTR = "for";
              };
              "svg-pos" = {
                ELEMENT = "svg";
                ALLOW_ATTR = "style";
                REGEXP = "^position: absolute";
              };
            } //
              # allow styling via CSS classes for several elements
              (mergeAttrsList (map (e: {
                "${e}" = {
                  ELEMENT = e;
                  ALLOW_ATTR = "class";
                  REGEXP = "";
                };
              }) (splitString " " "div pre svg span ul li label input dl a")));
          };
        });
      };
      # Have nginx forward the root domain to forgejo
      services.nginx = mkIf cfg.public {
        virtualHosts."${config.yann.services.web.domain}" = {
          # TODO: how to behave without subpath?
          locations = mkIf (cfg.subpath != null) {
            "^~ /${cfg.subpath}/" = {
              proxyPass = "http://127.0.0.1:${toString cfg.port}/";
            };
            "^~ /${cfg.subpath}" = {
              proxyPass = "http://127.0.0.1:${toString cfg.port}/";
            };
          };
        };
      };
      # Put link on homepage
      yann.services.web.server = mkIf cfg.public {
        enable = mkDefault true;
        homepage.enable = mkDefault true;
        # TODO: handle non-subpath case
        homepage.links."🔨 ${fcfg.settings.DEFAULT.APP_NAME} (Code, Data & Assets)" =
          mkIf (cfg.subpath != null) "/${cfg.subpath}";
      };
    }
    (mkIf (!isNull cfg.templateDir) { # 📝 Custom templates
      systemd.tmpfiles.rules =
        # ⚠️  The below warning also applies here
        [ "L+ '${fcfg.customDir}/templates' - - - - ${cfg.templateDir}" ];
    })
    (mkIf (!isNull cfg.logo) { # 🖼️ generate all custom logo versions
      systemd.tmpfiles.rules = let
        # untested, maybe we don't even need PNG versions of logo and favicon
        mkPng = img: size: flags:
          let filename = builtins.baseNameOf img;
          in (pkgs.runCommand "${filename}-${toString size}.png" { } ''
            (set -x;${getExe pkgs.inkscape} -o $out -w ${
              toString size
            } ${img} ${flags})
          '');
        png512 = mkPng cfg.logo 512 "";
        png180 = mkPng cfg.logo 180 "";
        apple = mkPng cfg.logo 180 "--export-png-color-mode=RGB_16";
      in [
        # ⚠️  This causes weird 'Detected unsafe path transition' errors and does not work
        # unless everything upstream the stateDir is owned by root (I think!?)
        # I had the parent dir of stateDir chowned by my user and I couldn't get it to work.
        # Also using user's systemd.tmpfiles rules (couldn't even debug that, annoying systemd --user weirdness)
        "d '${fcfg.customDir}/public' 0750 ${fcfg.user} ${fcfg.group} - -"
        "d '${fcfg.customDir}/public/assets' 0750 ${fcfg.user} ${fcfg.group} - -"
        "d '${fcfg.customDir}/public/assets/img' 0750 ${fcfg.user} ${fcfg.group} - -"
        "L+ '${fcfg.customDir}/public/assets/img/logo.svg' - - - - ${cfg.logo}"
        "L+ '${fcfg.customDir}/public/assets/img/logo.png' - - - - ${png512}"
        "L+ '${fcfg.customDir}/public/assets/img/favicon.svg' - - - - ${cfg.logo}"
        "L+ '${fcfg.customDir}/public/assets/img/apple-touch-icon.png' - - - - ${apple}"
        "L+ '${fcfg.customDir}/public/assets/img/favicon.png' - - - - ${png180}"
      ];
    })
    (mkIf cfg.backup.enable {
      # Back up forgejo data regularly
      services.borgbackup.jobs.forgejo = {
        paths = config.services.forgejo.stateDir;
        repo = cfg.backup.to;
        encryption.mode = "none";
        compression = "auto,zstd";
        inhibitsSleep = true;
        # extraCreateArgs = [ "--progress" "--stats" ]; # TODO: currently broken, fix here: https://github.com/NixOS/nixpkgs/pull/332319
        startAt = "daily";
      };
      systemd.services.borgbackup-job-forgejo = rec {
        unitConfig.RequiresMountsFor =
          lib.mkForce [ cfg.backup.RequiresMountsFor ];
        conflicts = [ "forgejo.service" ]; # stop forgejo during backup
        after = conflicts; # start after forgejo is fully down
        onSuccess =
          conflicts; # start forgejo again afterwards (gives 'multiple trigger' warning though...)
        onFailure =
          conflicts; # start forgejo again afterwards (gives 'multiple trigger' warning though...)
        # The below works, but in other contexts has apperead to produce race conditions
        # serviceConfig.ExecStopPost = "+systemctl restart ${escapeShellArgs conflicts}"; # restart forgejo afterwards
      };
      systemd.timers."borgbackup-job-forgejo".timerConfig.RandomizedDelaySec =
        "1h";
    })
  ]);
}
