# system modules
import logging
import os
import sys
import json
from pathlib import Path
import argparse
import warnings
import zipfile

# external modules
os.environ["TF_CPP_MIN_LOG_LEVEL"] = "3"  # suppress tensorflow import warning
import keras
import numpy as np

logger = logging.getLogger(__name__)


# Function to print feature importance
def print_feature_importance(dense_layer, branch_name, input_columns, top_n=5):
    if dense_layer is None:
        print(f"No first Dense layer found for {branch_name} branch.")
        return
    kernel, bias = dense_layer.get_weights()
    avg_abs_weights = np.max(
        np.abs(kernel), axis=1
    )  # Average absolute weights per input feature
    sorted_indices = np.argsort(avg_abs_weights)[::-1]  # Descending order
    top_features = sorted_indices[:top_n]

    print(
        f"\nTop {top_n} features by average absolute weight in {dense_layer.name} ({branch_name} branch):"
    )
    for i, idx in enumerate(top_features, start=1):
        feature_name = (
            input_columns[idx] if idx < len(input_columns) else f"Feature_{idx}"
        )
        print(f"  {i}. {feature_name}: {avg_abs_weights[idx]:.4f}")


def print_model_structure(model, input_columns=None):
    """
    Print a concise summary of a multi-branch, multi-output model.

    Args:
        model (tf.keras.Model): The model to summarize
        input_columns (list, optional): List of input column names for feature labeling
    """

    def _format_layer_info(layer):
        """Helper to format layer information"""
        name = layer.name
        params = layer.count_params()
        out_shape = layer.output.shape
        return f"{name} ({layer.__class__.__name__})", params, out_shape

    def _print_branch_summary(branch_name, branch_layers):
        """Print summary for a specific branch"""
        print(f"\n{branch_name.upper()} BRANCH:")
        total_branch_params = 0
        for layer in branch_layers:
            layer_info, layer_params, out_shape = _format_layer_info(layer)
            print(f"  {layer_info:<30} Params: {layer_params:>7,}  Shape: {out_shape}")
            total_branch_params += layer_params
        print(f"  Total {branch_name.upper()} Branch Params: {total_branch_params:,}")

    # Overall model summary
    print("=== MODEL SUMMARY ===")
    print(f"Model Name: {model.name}")
    print(f"Total Parameters: {model.count_params():,}")

    # Identify and print branches
    xy_branch_layers = [l for l in model.layers if "branch_xy" in l.name]
    z_branch_layers = [l for l in model.layers if "branch_z" in l.name]
    output_layers = [l for l in model.layers if "output_" in l.name]

    # Print branch summaries
    _print_branch_summary("XY", xy_branch_layers)
    _print_branch_summary("Z", z_branch_layers)

    # Output layers
    print("\nOUTPUT LAYERS:")
    total_output_params = 0
    for layer in output_layers:
        layer_info, layer_params, out_shape = _format_layer_info(layer)
        print(f"  {layer_info:<30} Params: {layer_params:>7,}  Shape: {out_shape}")
        total_output_params += layer_params
    print(f"  Total Output Layer Params: {total_output_params:,}")

    # Optional input columns display
    if input_columns:
        print("\nINPUT COLUMNS:")
        for i, col in enumerate(input_columns, 1):
            print(f"  {i}. {col}")

        # Approximate feature importance
        print("\n=== APPROXIMATE INPUT FEATURE IMPORTANCE (NAIVE) ===")
        print(
            "This is a crude approximation based on the average absolute weights of the first Dense layer(s)."
        )
        print(
            "For more accurate methods, consider SHAP, LIME, or permutation importance.\n"
        )

        # Identify first Dense layers of each branch
        xy_first_dense = next(
            (l for l in xy_branch_layers if isinstance(l, keras.layers.Dense)), None
        )
        z_first_dense = next(
            (l for l in z_branch_layers if isinstance(l, keras.layers.Dense)), None
        )

        # Print feature importance
        print_feature_importance(xy_first_dense, "XY", input_columns)
        print_feature_importance(z_first_dense, "Z", input_columns)


parser = argparse.ArgumentParser(
    description="Show information about a PARASITE keras model file"
)
parser.add_argument(
    "modelfile", type=Path, help="The .parasite.keras model file to examine"
)
parser.add_argument(
    "-v", "--verbose", action="count", default=0, help="more -v → more logging output"
)

if __name__ == "__main__":
    args = parser.parse_args()
    logging.basicConfig(level=logging.WARNING - args.verbose * 10)
    logger.debug(f"{args = }")
    try:
        zf = zipfile.ZipFile(args.modelfile)
    except Exception as e:
        logger.critical(f"Couldn't open model file {args.modelfile} as ZIP: {e!r}")
        parser.exit(1)

    input_columns = None
    parasite_json = {}
    try:
        with zf.open(f := "parasite.json") as fh:
            parasite_json = json.load(fh)
        if not (input_columns := parasite_json.get(c := "input_columns")):
            logger.warning(f"{f} in {zf.filename} doesn't contain {c!r} key")
        if not (parameters := parasite_json.get(c := "parameters")):
            logger.warning(f"{f} in {zf.filename} doesn't contain {c!r} key")
    except Exception as e:
        logger.error(f"Couldn't read file {f} in {zf.filename}: {e!r}")

    logger.debug(f"files in {args.modelfile}: {[f.filename for f in zf.filelist]}")
    # zf.close()

    try:
        model = keras.saving.load_model(args.modelfile)
    except Exception as e:
        logger.error(f"Can't load model file {args.modelfile}: {e!r}")

    print_model_structure(model, input_columns=input_columns)

    if parasite_json:
        print(f"\n\n=== OTHER PARAMETERS ===\n")
        json.dump(parasite_json, sys.stdout, indent=4)
        print("\n")
