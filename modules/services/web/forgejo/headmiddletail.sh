#!/usr/bin/env bash
set -o errexit
set -o nounset
set -o pipefail

linenumbers () {
    nl -s ' | ' -b all
}

headmiddletail () {
  awk '
BEGIN {
    N = 100
    M = 1000
}

{
    line[NR % N] = $0

    if (NR <= N) {
        # Print the first N lines
        print $0
        LASTPRINTNR=NR
    } else if (NR > N && (NR - N) % M == 0) {
        # Print every M lines, indicating the number of skipped lines in-between
        # Afterwards, exponentially increase M
        print "          ... <" , NR - LASTPRINTNR - 1, "lines > ..."
        print $0
        LASTPRINTNR=NR
        M *= 2
    }
}

END {
    if (NR > 2 * N) {
        # If the file had more than 2*N lines print the number of skipped lines followed by the last N
        print "          ... <" , NR - N - LASTPRINTNR , "lines > ..."
        start = NR - N + 1
        for (i = start; i <= NR; i++) {
            print line[i % N]
        }
    } else if (NR > N) {
        # If the file had between N and 2*N lines just print the remaining
        start = N + 1 > NR - N + 1 ? N + 1 : NR - N + 1
        for (i = start; i <= NR; i++) {
            print line[i % N]
        }
    } # Otherwise, nothing to do, head printed everything already
}
'
}

linenumbers | headmiddletail
