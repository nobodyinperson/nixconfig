{ pkgs, config, lib, ... }:
let cfg = config.yann.services.web.jupyterhub;
in with lib; {
  options.yann.services.web.jupyterhub = {
    enable = mkEnableOption "JupyterHub server";
    public = mkEnableOption "make JupyterHub publicly accessible";
    port = mkOption {
      description = "local JupyterHub port";
      type = types.port;
      default = 9000; # use something else than what manual jupyter would use
    };
    subpath = mkOption {
      description = "subpath for JupyterHub";
      type = types.str;
      default = "jupyter";
    };
  };
  config = mkIf cfg.enable {
    # Run JupyterHub
    services.jupyterhub = {
      enable = true;
      port = cfg.port;
      extraConfig = ''
        c.JupyterHub.bind_url = "http://127.0.0.1:${
          toString cfg.port
        }/${cfg.subpath}/"
      '';
    };
    # Proxy from outside subpath to local port
    services.nginx = mkIf cfg.public {
      virtualHosts."${config.yann.services.web.domain}" = {
        locations."/${cfg.subpath}" = {
          proxyPass = "http://localhost:${toString cfg.port}";
        };
      };
    };
    # Put link on homepage
    yann.services.web.server = mkIf cfg.public {
      enable = mkDefault true;
      homepage.enable = mkDefault true;
      homepage.links."🐍 Jupyter" = "/${cfg.subpath}";
    };
  };
}
