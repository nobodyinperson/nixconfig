{ pkgs, config, lib, utils, ... }:
with lib;
let cfg = config.yann.services.web.netcup-setdns;
in {
  options.yann.services.web.netcup-setdns = {
    enable = mkEnableOption "netcup.de DNS record updater";
    package = mkPackageOption pkgs "netcup-setdns" { };
    interval = mkOption {
      type = types.str;
      description =
        "Systemd calendar expression for updating the value via netcup's API";
      default = "daily";
      example = "*:0/15";
    };
    updates = mkOption {
      type = with types;
        listOf (attrsOf (submodule {
          options = {
            value = mkOption {
              type = str;
              description = ''
                The value to update to. "public-ipv4" (the default) or "public-ipv6" means to use the current external address.'';
              default = "public-ipv4";
              example = "1.2.3.4";
            };
            type = mkOption {
              type = str;
              description = "the DNS record type";
              default = "A";
              example = "AAAA";
            };
            host = mkOption {
              type = str;
              description = "the DNS record host";
              default = "*";
              example = "@";
            };
          };
        }));
      description =
        "list of mapping from domains to DNS record type and host to update with current external IPv4";
      example = ''
        [
          { "mydomain.de" = { value = "1.2.3.4"; }; } # hard-coded value
          { # updates A record for all subdomains to current external IPv4 address
            "thedomain.de".value = "public-ipv4";
            "thedomain.de".type = "A";
            "thedomain.de".host = "*";
          }
          # updates A record for all subdomains to current external IPv4 address
          { "thatdomain.org".host = "@"; }
          { # updates AAAA record for all subdomains to current external IPv6 address
            "otherdomain.com".value = "public-ipv6";
            "otherdomain.com".type = "AAAA";
          }
        ]
      '';
      default = [ ];
    };
    environmentFile = mkOption {
      type = with types; nullOr path;
      description = ''
        An environment file as defined in systemd.exec(5), defining
        APIKEY (API key), APIPW (API password) and CID (customer id) as listed in the CCP.
      '';
      example = "/tmp/netcup.env";
    };
  };
  config = mkIf cfg.enable {
    systemd = let
      updateToArgsList =
        (mapAttrsToList (domain: records: records // { inherit domain; }));
      # [ { domain=, type=, value=, host=... } ... ]
      listOfArgs = flatten (map updateToArgsList cfg.updates);
      argsToSystemd = { domain, type, value, host, ... }@args:
        let
          # have to escape * etc. otherwise NixOS build error in system-units
          serviceName = concatStringsSep "-" ([ "netcup-setdns" ]
            ++ map utils.escapeSystemdPath (builtins.attrValues args));
          serviceDesc =
            "Set ${type} DNS record for domain ${domain} and host ${host} to ${
              {
                "public-ipv4" = "the public IPv4 address";
                "public-ipv6" = "the public IPv6 address";
              }."${value}" or value
            } via Netcup CCP API";
        in {
          services.${serviceName} = {
            description = serviceDesc;
            after = [ "network-online.target" ];
            wants = [ "network-online.target" ];
            serviceConfig = {
              EnvironmentFile = cfg.environmentFile;
              Restart = "on-failure";
              RestartSec = "1min";
            };
            startAt = cfg.interval;
            path = [ pkgs.curl pkgs.perl cfg.package ];
            script = "${getExe cfg.package} ${domain} ${type} ${value} ${host}";
          };
          timers.${serviceName} = {
            description = "Regularly ${serviceDesc}";
            timerConfig.RandomizedDelaySec = 30;
            timerConfig.OnStartupSec = 30;
          };
        };
      updateServices = map argsToSystemd listOfArgs;
    in mkMerge updateServices;
  };
}
