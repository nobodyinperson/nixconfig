{ pkgs, config, ... }: {
  imports = [ ./web ./mqtt.nix ./scanshop.nix ./cpufreq-schedule.nix ];
}
