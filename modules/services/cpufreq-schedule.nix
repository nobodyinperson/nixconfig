{ pkgs, config, lib, ... }:
with lib;
let cfg = config.yann.services.cpufreq-schedule;
in {
  options.yann.services.cpufreq-schedule = {
    enable = mkEnableOption "time schedule for cpu frequency";
    times = mkOption {
      type = with types; attrsOf str;
      description = "mapping of systemd times to cpu frequencies";
      example = ''
        {
           "*-*-* 18:00:00" = "2GHz"; # throttle for the night to be quiet
           "*-*-* 08:00:00" = "6GHz"; # during the day a bit of noise is fine
        }
      '';
    };
  };
  config = mkIf cfg.enable {
    environment.systemPackages = [
      config.boot.kernelPackages.cpupower # make 'cpupower' command available
    ];
    systemd.services = mapAttrs' (time: cpufreq:
      nameValuePair "cpu-max-${cpufreq}" {
        description = "Allow up to ${cpufreq} for CPU";
        startAt = time;
        serviceConfig = {
          Type = "oneshot";
          ExecStart = "${
              getExe config.boot.kernelPackages.cpupower
            } -c all frequency-set --max ${escapeShellArg cpufreq}";
        };
      }) cfg.times;
  };
}
