{ pkgs, config, lib, ... }:
let cfg = config.yann.services.mqtt;
in with lib; {
  options.yann.services.mqtt = {
    enable = mkEnableOption "MQTT broker";
    public = mkEnableOption "make MQTT broker accessible from outside";
  };
  config = mkIf cfg.enable {
    services.mosquitto = {
      enable = true;
      listeners = [{
        acl = [ "pattern readwrite #" ];
        omitPasswordAuth = true;
        settings.allow_anonymous = true;
      }];
    };
    networking.firewall = mkIf cfg.public {
      enable = true;
      allowedTCPPorts = [ 1883 ];
    };
    environment.systemPackages = with pkgs; [ mosquitto ];
  };
}
