{ pkgs, config, lib, ... }@args: {
  # 🏡 Home-Manager
  home-manager.useGlobalPkgs = true; # 🤷 more efficient somehow
  home-manager.useUserPackages = true; # 🤷
  # if there are conflicting files already, back them up with timestamp,
  # without timestamp, home-manager refuses to work (https://github.com/nix-community/home-manager/issues/4199)
  home-manager.backupFileExtension = "${toString args.self.lastModified}.bak";
  home-manager.verbose = true; # more logs (journalctl -efu home-manager-$USER)
  home-manager.sharedModules = [
    ({ pkgs, config, lib, osConfig, ... }:
      with lib; {
        # 🔧 XFCE settings. Alles aus dem Programm „Einstellungsbearbeitung” genommen.
        xfconf.settings =
          lib.mkIf osConfig.services.xserver.desktopManager.xfce.enable {
            # 🎨 Theme
            xfwm4."general/theme" = "Matcha-sea";
            xsettings."Net/IconThemeName" = "Papirus-Maia";
            xsettings."Net/ThemeName" = "Matcha-sea";
            xsettings."Gtk/CursorThemeName" = "breeze_cursors";
            xsettings."Gtk/FontName" = "Atkinson Hyperlegible 11";
            xsettings."Gtk/MonospaceFontName" = "Jetbrains Mono Medium 10";
            # sonstiges
            xfwm4."general/workspace_count" = "4";
            xfwm4."general/title_font" = "Atkinson Hyperlegible 11";
            # nicht durch Scrollen oben das Fenster „Einrollen”
            xfwm4."general/mousewheel_rollup" = false;
            # kein "S" zum „Einrollen” des Fensters
            xfwm4."general/button_layout" = "O|HMC";
            # beim Abmelden nicht die Session speichern, kann manchmal Probleme machen
            xfce4-session."general/SaveOnExit" = false;
            # Präsentations-Modus, Bildschirm nicht irgendwie aus machen
            xfce4-power-manager."xfce4-power-manager.presentation-mode" = true;
            # ⌨️ Keyboard shortcuts
            xfce4-keyboard-shortcuts."commands/custom/<Alt>F2" =
              "rofi -show drun";
            xfce4-keyboard-shortcuts."commands/custom/<Primary><Alt>T" =
              "xfce4-terminal";
            xfce4-keyboard-shortcuts."commands/custom/<Primary><Shift>W" =
              "passrofi";
            xfce4-keyboard-shortcuts."commands/custom/<Super>S" = "simple-scan";
            xfce4-keyboard-shortcuts."commands/custom/<Primary><Alt>Delete" =
              "xflock4";
            # Tiling keyboard shortcuts
            xfce4-keyboard-shortcuts."/xfwm4/default/<Super>KP_Down" =
              "tile_down_key";
            xfce4-keyboard-shortcuts."/xfwm4/default/<Super>KP_End" =
              "tile_down_left_key";
            xfce4-keyboard-shortcuts."/xfwm4/default/<Super>KP_Home" =
              "tile_up_left_key";
            xfce4-keyboard-shortcuts."/xfwm4/default/<Super>KP_Left" =
              "tile_left_key";
            xfce4-keyboard-shortcuts."/xfwm4/default/<Super>KP_Next" =
              "tile_down_right_key";
            xfce4-keyboard-shortcuts."/xfwm4/default/<Super>KP_Page_Up" =
              "tile_up_right_key";
            xfce4-keyboard-shortcuts."/xfwm4/default/<Super>KP_Right" =
              "tile_right_key";
            xfce4-keyboard-shortcuts."/xfwm4/default/<Super>KP_Up" =
              "tile_up_key";
          };
        # 🐺 LibreWolf browser
        programs.librewolf = {
          enable = true;
          languagePacks = [ "de" "en-US" ];
          settings = {
            # behavior
            "browser.tabs.closeWindowWithLastTab" = false;
            "browser.download.useDownloadDir" = false; # ask where to download!
            "privacy.resistFingerprinting" = true;
            "dom.security.https_only_mode" =
              false; # allow http, need it for development
            # 🚀 performance
            "gfx.webrender.all" = true;
            "media.ffmpeg.vaapi.enabled" = true;
            # 🌐 language
            "browser.translations.neverTranslateLanguages" = "de,en";
            "intl.locale.requested" = "de,en";
            # don't be too pedantic and keep logins working across launches
            "privacy.clearOnShutdown.cache" = false;
            "privacy.clearOnShutdown.cookies" = false;
            "privacy.clearOnShutdown.downloads" = false;
            # 🔐 remember and prefill passwords
            "signon.rememberSignons" = true;
            "signon.autofillForms" = true;
            # might have something important
            "privacy.clearOnShutdown.formdata" = false;
            # enable firefox sync
            "identity.fxaccounts.enabled" = true;
          };
        };
        # 📁 Thunar Custom Actions 🖱️
        programs.thunar.customActions = mkIf osConfig.programs.thunar.enable {
          enable = true;
          actions = [
            {
              name = "Terminal hier öffnen";
              icon = "utilities-terminal";
              command =
                "exo-open --working-directory %f --launch TerminalEmulator";
              description = "Terminal in diesem Ordner öffnen";
              range = "1";
              directories = true;
            }
            {
              name = "Datei schicken";
              icon = "document-send";
              command = ''
                xfce4-terminal --working-directory %d --hold --execute ${
                  getExe pkgs.magic-wormhole
                } send %n
              '';
              description = "Diese Datei mit Magic Wormhole verschicken";
              range = "1";
              submenu = "🪱 Magic Wormhole";
              directories = false;
              text-files = true;
              audio-files = true;
              video-files = true;
              image-files = true;
              other-files = true;
            }
            {
              name = "Receive file";
              icon = "inbox";
              command = ''
                xfce4-terminal --working-directory %f --hold --execute ${
                  getExe pkgs.magic-wormhole
                } receive
              '';
              submenu = "🪱 Magic Wormhole";
              description =
                "Eine Datei mit Magic Wormhole in diesen Ordner empfangen";
              range = "1";
              directories = true;
            }
          ];
        };
        # 👻📟 Ghostty Terminal
        programs.ghostty = {
          enable = true;
          installVimSyntax = true;
          installBatSyntax = true;
          enableFishIntegration = true;
          settings = {
            theme = "Builtin Solarized Light";
            font-family = [ "Jetbrains Mono" "Twitter Color Emoji" ];
          };
        };
      })
  ];
}
