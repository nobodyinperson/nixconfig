{ pkgs, config, lib, ... }:
let userName = "yann";
in {
  users.users.${userName} = {
    isNormalUser = true;
    description = "Yann Büchau";
    extraGroups = [
      "networkmanager"
      "wheel"
      userName
      "scanner"
      "lp"
      "audio"
      "dialout"
      "input"
      "video"
    ];
    packages = with pkgs; [ latexmk-yann ];
    # start with a certain password (to make VMs work), changed live later
    initialHashedPassword = config.yann.secrets.passwords.hashedPassword1;
    shell = pkgs.fish;
  };
  # start with a certain password (to make VMs work), changed live later
  users.users.root.initialHashedPassword =
    lib.mkDefault config.yann.secrets.passwords.hashedPassword2;
  nix.settings.trusted-users = [ userName ];
  # 🏡 Home Manager
  home-manager.users.${userName} = {
    # dummy to activate sharedModules as well
  };
}
