{ pkgs, ... }: {
  environment.systemPackages = with pkgs; [ system-config-printer ];
  # Enable CUPS to print documents.
  services.printing.enable = true;
  services.printing.drivers = with pkgs; [ cups-kyodialog brlaser ];
  services.avahi.enable = true;
  services.avahi.nssmdns4 = true;
  # for a WiFi printer
  # services.avahi.openFirewall = true;
}
