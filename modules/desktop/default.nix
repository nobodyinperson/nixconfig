{ ... }: {
  imports = [
    ./styli.sh.nix
    ./desktop.nix
    ./printing.nix
    ./gnome-boxes.nix
    ./thumbnailers
  ];
}
