{ pkgs, options, config, lib, ... }:
let cfg = config.yann.desktop;
in with lib; {
  imports = [ ./styli.sh.nix ./gnome-boxes.nix ];

  options.yann.desktop = { enable = mkEnableOption "Enable Yann's desktop"; };

  config = mkIf cfg.enable {
    # 📢 Enable sound with pipewire.
    hardware.pulseaudio.enable = false;
    hardware.pulseaudio.extraConfig = ''
      load-module module-switch-on-connect
      load-module module-switch-on-port-available
    '';
    security.rtkit.enable = true;
    services.pipewire = {
      enable = true;
      alsa.enable = true;
      alsa.support32Bit = true;
      pulse.enable = true;
      extraConfig.pipewire = {
        "99-disable-bell" = {
          "context.properties" = { "module.x11.bell" = false; };
        };
      };
    };
    programs.noisetorch.enable = true; # deep-learning noise suppression

    yann.desktop.programs.gnome-boxes.enable = mkDefault true;
    # yann.desktop.stylish.enable = mkDefault true; # it seems styli.sh is dead as Unsplash.com doesn't provide the url anymore

    # Enable touchpad support (enabled default in most desktopManager).
    # services.xserver.libinput.enable = true;

    # Enable the X11 windowing system.
    services.xserver.enable = true;

    # Enable the XFCE Desktop Environment.
    services.xserver.displayManager.lightdm.enable = true;
    services.xserver.desktopManager.xfce.enable = true;
    programs.thunar.enable = true;
    programs.thunar.plugins = with pkgs.xfce; [
      thunar-archive-plugin
      thunar-media-tags-plugin
      thunar-volman
    ];

    services.gvfs.enable = true;
    services.tumbler.enable = true;

    # GNOME keyring for Evolution?
    security.pam.services.gdm.enableGnomeKeyring = true;
    services.gnome.gnome-keyring.enable = true;

    # Configure keymap in X11
    services.xserver.xkb = {
      layout = "de";
      variant = "";
    };

    services.teamviewer.enable = true;

    fonts = {
      packages = with pkgs; [
        atkinson-hyperlegible
        twitter-color-emoji
        jetbrains-mono
        noto-fonts
        (nerdfonts.override { fonts = [ "Noto" ]; })
      ];
      fontconfig.defaultFonts = {
        sansSerif = [ "Atkinson Hyperlegible" ];
        monospace = [ "Jetbrains Mono" ];
        emoji = [ "Twitter Color Emoji" ];
      };
    };

    # 🫓📦 Flatpak support
    services.flatpak.enable = true;
    xdg.portal = {
      enable = true;
      extraPortals = with pkgs; [ xdg-desktop-portal-gtk ];
    };

    # Scanning
    hardware.sane.enable = true;

    # 🔵🦷 Bluetooth 
    hardware.bluetooth.enable = true;
    services.blueman.enable = true;

    environment.systemPackages = with pkgs;
      let
        theming =
          [ matcha-gtk-theme papirus-maia-icon-theme libsForQt5.breeze-gtk ];
        xfceStuff = with xfce; [
          xfce4-pulseaudio-plugin
          xfce4-whiskermenu-plugin
          xfce4-fsguard-plugin
        ];
        desktopApps = [
          librewolf
          qalculate-gtk
          mate.engrampa
          gnome-disk-utility
          meld
          filelight
          recoll # file indexing / search
          watchmate # for PineTime smartwatch
          signal-desktop
          thunderbird
          rustdesk
          zoom-us # ugh...
          webex # ugh no webinterface anymore
        ];
        miscPackages = [
          redshift
          rofi
          passrofi
          bemoji
          pavucontrol
          xclip
          xsel
          gnome-keyring
          bemoji
          passrofi
        ];
      in theming ++ desktopApps ++ xfceStuff ++ miscPackages;
  };
}
