# styli.sh doesn't work anymore as Unsplash.com disabled their /random endpoint
{ pkgs, config, lib, ... }:
let
  stylish = pkgs.stylish.overrideAttrs (final: prev: {
    postInstall = with pkgs; ''
      wrapProgram $out/bin/styli.sh --prefix PATH : ${
        lib.makeBinPath [ curl feh file jq util-linux wget xfce.xfconf ]
      }
    '';
  });
  cfg = config.yann.desktop.stylish;
in with lib; {
  options.yann.desktop.stylish = {
    enable = mkEnableOption "Random wallpapers";
    flags = mkOption {
      type = types.str;
      description = "styli.sh cli flags";
      default = "--xfce --search sun,field --width 2560 --height 1920";
    };
  };
  config = mkIf (config.yann.desktop.enable && cfg.enable) {
    ### Styli.sh Auto Wallpaper Change ###
    systemd.user.timers."styli.sh" = {
      description = "Styli.sh - Switch Wallpaper";
      wantedBy = [ "default.target" ];
      timerConfig = {
        OnCalendar = "*-*-* *:00:00";
        Unit = "styli.sh.service";
      };
    };
    systemd.user.services."styli.sh" = {
      description = "Styli.sh - Switch Wallpaper";
      path = [ pkgs.file pkgs.gawk ]; # needed for some reason
      environment.STYLISH_FLAGS = cfg.flags;
      script = ''
        ${stylish}/bin/styli.sh $STYLISH_FLAGS
      '';
      serviceConfig = { Type = "oneshot"; };
      wantedBy = [ "default.target" ];
    };
    environment.systemPackages = [ stylish ];
  };
}
