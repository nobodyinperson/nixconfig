{ pkgs, config, lib, ... }:
let cfg = config.yann.desktop.thumbnailers;
in with lib; {
  options.yann.desktop.thumbnailers = {
    enable = mkEnableOption "custom thumbnailers";
    entries = mkOption {
      description = "thumbnailer entries";
      example = ''
        {
          # svg thumbnailer
          svg.mimeTypes = [ "image/svg+xml" ];
          svg.cmd = "''${pkgs.inkscape}/bin/inkscape -w %s -h %s -o %o %i";
          # custom thumbnailer
          myThumbnailer.mimeTypes = [ "my/x-type" ];
          myThumbnailer.cmd = "mycmd --input %i --size %s --output %o";
        }
      '';
      type = with types;
        attrsOf (submodule {
          options = {
            mimeTypes = mkOption {
              type = with types; listOf str;
              description = "MIME types this thumbnailer should be used for";
            };
            cmd = mkOption {
              type = types.str;
              description = ''
                command to create the thumbnail.
                Placeholders: %u: uri of file to thumbnail, %i: path to file to thumbnail, %o: thumbnail output file, %s: max thumbnail pixel size, %%: literal percent sign
              '';
            };
          };
        });
    };
  };
  config = let
    thumbnailers = mapAttrsToList (name: thumbnailer:
      pkgs.writeTextFile {
        name = "${name}-thumbnailer";
        destination = "/share/thumbnailers/${name}.thumbnailer";
        text = ''
          [Thumbnailer Entry]
          Exec=${thumbnailer.cmd}
          MimeType=${concatStringsSep ";" thumbnailer.mimeTypes}
        '';
      }) cfg.entries;
  in mkIf cfg.enable {
    nixpkgs.overlays = [
      # custom thumbnailer packages
      (final: prev: {
        gcode-thumbnailer = pkgs.stdenv.mkDerivation {
          name = "gcode-thumbnailer";
          src = ./gcode-thumbnailer;
          buildInputs = [ pkgs.python3 ];
          dontUnpack = true;
          installPhase = ''
            install -Dm 777 $src $out/bin/gcode-thumbnailer
          '';
        };
      })
    ];
    environment.systemPackages = thumbnailers;
  };
}
