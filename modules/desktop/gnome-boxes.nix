{ pkgs, config, lib, ... }:
let cfg = config.yann.desktop.programs.gnome-boxes;
in with lib; {
  options.yann.desktop.programs.gnome-boxes.enable =
    mkEnableOption "Enable Gnome Boxes for VMs";
  config = lib.mkIf (config.yann.desktop.enable && cfg.enable) {
    virtualisation.libvirtd.enable = true;
    users.extraGroups.vboxusers.members = [ "yann" ];
    boot.kernelModules = [ "kvm-amd" "kvm-intel" ];
    environment.systemPackages = with pkgs; [ gnome-boxes ];
    virtualisation.spiceUSBRedirection.enable = true;
  };
}
