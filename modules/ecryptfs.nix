{ pkgs, config, lib, ... }:
with lib;
let cfg = config.yann.ecryptfs;
in {
  options.yann.ecryptfs = {
    enable = mkEnableOption
      "ecryptfs home dir encryption settings (manual migration still needed though)";
  };
  config = mkIf cfg.enable {
    security.pam.enableEcryptfs = true;
    environment.systemPackages = with pkgs; [ ecryptfs ];
    boot.kernelModules = [ "ecryptfs" ];
  };
}
